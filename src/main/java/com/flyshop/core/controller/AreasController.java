package com.flyshop.core.controller;

import com.flyshop.core.entity.DataVo;
import com.flyshop.module.webconfig.model.Areas;
import com.flyshop.module.webconfig.service.AreasService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 开发公司：28844.com<br/>
 * 版权：28844.com<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 10:56 2018/7/7
 */
@Controller
public class AreasController {
    @Autowired
    protected AreasService srv;

    //按父级id查询id下所有地区列表
    @ResponseBody
    @RequestMapping(value = "/areas/area_child")
    public List<Areas> selectAreasByPid(@RequestParam(value = "parentId", defaultValue = "0") int parentId){
        List<Areas> areas=srv.selectAreasByPid(parentId);
        return areas;
    }
}
