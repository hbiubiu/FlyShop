package com.flyshop.core.base;

import com.flyshop.common.utils.AdminSessionUtil;
import com.flyshop.common.utils.UserSessionUtil;
import com.flyshop.module.admin.model.Admin;
import com.flyshop.module.admin.service.AdminService;
import com.flyshop.module.template.service.TemplateService;
import com.flyshop.module.user.model.User;
import com.flyshop.module.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ModelAttribute;


import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Controller基类
 * 
 * @author sunkaifei
 */
public class BaseController {
    @Resource
    protected HttpServletRequest request;
    @Resource
    protected HttpServletResponse response;
	@Autowired
	protected AdminService adminService;
    @Autowired
	protected UserService userService;
    @Resource
    protected HttpSession session;
	@Autowired
	protected TemplateService theme;

	  /**
	   * 获取用户信息
	   *
	   * @return
	   */
	protected User getUser() {
		User user = UserSessionUtil.getLoginMember(request);
		if (StringUtils.isEmpty(user)) {
			return null;
		} else {
			return userService.findByUsername(user.getUsername());
		}
	}

	/**
	 * 获取用户信息
	 *
	 * @return
	 */
	protected Admin getAdminUser() {
		Admin user = AdminSessionUtil.getLoginMember(request);
		if (StringUtils.isEmpty(user)) {
			return null;
		} else {
			return adminService.findByUsername(user.getAdminName());
		}
	}
}
