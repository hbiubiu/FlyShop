/*
 *	Copyright © 2015 Zhejiang SKT Science Technology Development Co., Ltd. All rights reserved.
 *	浙江斯凯特科技发展有限公司 版权所有
 *	http://www.28844.com
 */

package com.flyshop.core.base;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;

import com.flyshop.common.utils.StringHelperUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ApplicationObjectSupport;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateModelException;

@Service
public abstract class AbstractTagPlugin extends ApplicationObjectSupport implements
		TemplateDirectiveModel, Plugin {
	@Autowired
	protected HttpServletRequest request;

	@Autowired
	private FreeMarkerConfigurer freeMarkerConfigurer;
	
	@Override
	@PostConstruct
	public void init() throws TemplateModelException {
		String className = this.getClass().getName().substring(this.getClass().getName().lastIndexOf(".") + 1);
		String beanName = StringUtils.uncapitalize(className);
		String tagName = "fly_" + StringHelperUtil.toUnderline(beanName);
		freeMarkerConfigurer.getConfiguration().setSharedVariable(tagName, this.getApplicationContext().getBean(beanName));
	}
}
