package com.flyshop.web.front;

import com.flyshop.common.utils.LocaleMessageSourceUtil;
import com.flyshop.core.base.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import javax.servlet.http.HttpServletResponse;
import java.util.Locale;

/**
 * 开发公司：28844.com<br/>
 * 版权：28844.com<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 2018-7-2
 */
@Controller
public class IndexController extends BaseController {
    private static Logger logger = LoggerFactory.getLogger(IndexController.class);
    @Autowired
    private LocaleMessageSourceUtil messageSourceUtil;

    /**
     * 中、英区域转换控制器
     * @param response
     * @param language
     * @return
     */
    @GetMapping("/checklanguage/{language}")
    public String  language( HttpServletResponse response,@PathVariable String language,ModelMap modelMap){
        //打印日志
        Locale locale= request.getLocale();
        //logger.error("----------------"+locale.toString());
       // LocaleResolver localeResolver = RequestContextUtils.getLocaleResolver(request);
        language=language.toLowerCase();
        logger.info("language:"+language);
        if(language==null||language.equals("")){
            return "redirect:/";
        }else{
            if(language.equals("cn")){
                request.getSession().setAttribute(SessionLocaleResolver.LOCALE_SESSION_ATTRIBUTE_NAME, new Locale("zh", "CN"));
            }else if(language.equals("us")){
                request.getSession().setAttribute(SessionLocaleResolver.LOCALE_SESSION_ATTRIBUTE_NAME, new Locale("en", "US"));
            }else{
                request.getSession().setAttribute(SessionLocaleResolver.LOCALE_SESSION_ATTRIBUTE_NAME, new Locale("zh", "CN"));
            }
        }
        return "redirect:/";
    }

    /**
     * 首页
     *
     * @return
     */
    @GetMapping(value = {"/" , "/index"})
    public String index(ModelMap modelMap){
        String welcome = messageSourceUtil.getMessage("welcome");
        //System.out.println("-----------------"+welcome);
        modelMap.addAttribute("admin", "dggdfggdfdfdgdf");
        return theme.getPcTemplate("index");
    }

}
