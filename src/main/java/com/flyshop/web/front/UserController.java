package com.flyshop.web.front;

import com.flyshop.common.utils.UserSessionUtil;
import com.flyshop.constant.Const;
import com.flyshop.core.base.BaseController;
import com.flyshop.core.entity.DataVo;
import com.flyshop.module.user.model.User;
import com.flyshop.module.user.service.UserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class UserController extends BaseController {
    @Autowired
    protected UserService srv;

    //用户注册
    @GetMapping(value = "/reg")
    public String userReg(){
        return theme.getPcTemplate("user/reg");
    }

    /**
     * 添加新用户
     *
     * @param username
     * @param password
     * @param modelMap
     * @return
     */
    @ResponseBody
    @PostMapping(value = "/ucenter/reg_user")
    public DataVo addUser(@RequestParam(value = "username", required = false) String username,
                          @RequestParam(value = "password", required = false) String password,
                          @RequestParam(value = "password2", required = false) String password2,
                          @RequestParam(value = "captcha", required = false) String captcha,
                          ModelMap modelMap) {
        DataVo data = DataVo.failure("操作失败");
        try {
            username=username.trim();
            password=password.trim();
            password2=password2.trim();
            captcha=captcha.trim();
            String kaptcha = (String) session.getAttribute(Const.KAPTCHA_SESSION_KEY);
            // 校验验证码
            if (captcha == null && "".equals(captcha)) {
                return DataVo.failure("验证码不能为空");
            }
            captcha=captcha.toLowerCase();
            if(!captcha.equals(kaptcha)){
                return DataVo.failure("验证码错误");
            }

            if (StringUtils.isBlank(username)) {
                return DataVo.failure("用户名不能为空");
            }
            if (StringUtils.isBlank(password)) {
                return DataVo.failure("密码不能为空");
            }
            if (password.length() < 6) {
                return DataVo.failure("密码不能小于6位");
            }
            if (password.length() > 16) {
                return DataVo.failure("密码不能大于16位");
            }
            if (!password.equals(password2)) {
                return DataVo.failure("密码两次输入不一致");
            }
            srv.addUserReg(username, password,request);
            return DataVo.success("操作成功");
        } catch (Exception e) {
            data = DataVo.failure(e.getMessage());
        }
        return data;
    }

    //用户登录页面
    @GetMapping(value = "/login")
    public String userLogin(@RequestParam(value = "redirectUrl",required = false) String redirectUrl,ModelMap modelMap){
        if(getUser() != null){
            return "redirect:/ucenter/index";
        }
        modelMap.addAttribute("redirectUrl",redirectUrl);
        return theme.getPcTemplate("user/login");
    }

    //登录处理
    @ResponseBody
    @PostMapping(value = "/ucenter/login_act")
    public DataVo userLogin(
            @RequestParam(value = "username", required = false) String username,
            @RequestParam(value = "password", required = false) String password,
            @RequestParam(value = "redirectUrl",required = false) String redirectUrl,
            @RequestParam(value = "captcha", required = false) String captcha) {
        try {
            String kaptcha = (String) session.getAttribute("kaptcha");
            if (StringUtils.isBlank(username)) {
                return DataVo.failure("用户名不能为空");
            }
            if (StringUtils.isBlank(password)) {
                return DataVo.failure("密码不能为空");
            } else if (password.length() < 6 && password.length() > 30) {
                return DataVo.failure("密码最少6个字符，最多30个字符");
            }
            // 校验验证码
            if (captcha != null) {
                if (!captcha.equalsIgnoreCase(kaptcha)) {
                    return DataVo.failure("验证码错误");
                }
            }else{
                return DataVo.failure("验证码不能为空");
            }
            User entity = srv.userLogin(username,password,request);
            if(entity==null){
                return DataVo.failure("帐号或密码错误。");
            }else{
                session.removeAttribute(Const.KAPTCHA_SESSION_KEY);
                if (StringUtils.isNotEmpty(redirectUrl)){
                    return DataVo.jump("操作成功", redirectUrl);
                }
                return DataVo.jump("操作成功", "/ucenter/index");
            }
        } catch (Exception e) {
            return DataVo.failure("帐号或密码错误。");
        }
    }

    //我的首页
    @GetMapping(value = "/ucenter/index")
    public String userIndex(ModelMap modelMap){
        if (getUser() != null) {
            modelMap.addAttribute("user", getUser());
        }
        return theme.getPcTemplate("user/index");
    }

    //我的订单
    @GetMapping(value = "/ucenter/order")
    public String userOrder(ModelMap modelMap){
        if (getUser() != null) {
            modelMap.addAttribute("user", getUser());
        }
        return theme.getPcTemplate("user/order");
    }

    //我的积分
    @GetMapping(value = "/ucenter/integral")
    public String userIntegral(ModelMap modelMap){
        if (getUser() != null) {
            modelMap.addAttribute("user", getUser());
        }
        return theme.getPcTemplate("user/integral");
    }

    //我的代金券
    @GetMapping(value = "/ucenter/redpacket")
    public String userRedpacket(ModelMap modelMap){
        if (getUser() != null) {
            modelMap.addAttribute("user", getUser());
        }
        return theme.getPcTemplate("user/redpacket");
    }

    //我的退款申请
    @GetMapping(value = "/ucenter/refunds")
    public String userRefunds(ModelMap modelMap){
        if (getUser() != null) {
            modelMap.addAttribute("user", getUser());
        }
        return theme.getPcTemplate("user/refunds");
    }

    //我的网站建议
    @GetMapping(value = "/ucenter/complain")
    public String userComplain(ModelMap modelMap){
        if (getUser() != null) {
            modelMap.addAttribute("user", getUser());
        }
        return theme.getPcTemplate("user/complain");
    }

    //我的商品咨询
    @GetMapping(value = "/ucenter/consult")
    public String userConsult(ModelMap modelMap){
        if (getUser() != null) {
            modelMap.addAttribute("user", getUser());
        }
        return theme.getPcTemplate("user/consult");
    }

    //我的商品评价
    @GetMapping(value = "/ucenter/evaluation")
    public String userEvaluation(ModelMap modelMap){
        if (getUser() != null) {
            modelMap.addAttribute("user", getUser());
        }
        return theme.getPcTemplate("user/evaluation");
    }

    //我的商品评价 -- 已评价
    @GetMapping(value = "/ucenter/isevaluation")
    public String userIsevaluation(ModelMap modelMap){
        if (getUser() != null) {
            modelMap.addAttribute("user", getUser());
        }
        return theme.getPcTemplate("user/isevaluation");
    }

    //我的站内短信
    @GetMapping(value = "/ucenter/message")
    public String userMessage(ModelMap modelMap){
        if (getUser() != null) {
            modelMap.addAttribute("user", getUser());
        }
        return theme.getPcTemplate("user/message");
    }

    //我的产品收藏
    @GetMapping(value = "/ucenter/favorite")
    public String userFavorite(ModelMap modelMap){
        if (getUser() != null) {
            modelMap.addAttribute("user", getUser());
        }
        return theme.getPcTemplate("user/favorite");
    }

    //我的账户余额
    @GetMapping(value = "/ucenter/account_log")
    public String userAccount_log(ModelMap modelMap){
        if (getUser() != null) {
            modelMap.addAttribute("user", getUser());
        }
        return theme.getPcTemplate("user/account_log");
    }

    //我的账户余额体现申请
    @GetMapping(value = "/ucenter/withdraw")
    public String userWithdraw(ModelMap modelMap){
        if (getUser() != null) {
            modelMap.addAttribute("user", getUser());
        }
        return theme.getPcTemplate("user/withdraw");
    }

    //我的在线充值
    @GetMapping(value = "/ucenter/online_recharge")
    public String userOnlineRecharge(ModelMap modelMap){
        if (getUser() != null) {
            modelMap.addAttribute("user", getUser());
        }
        return theme.getPcTemplate("user/online_recharge");
    }

    //我的收货地址管理
    @GetMapping(value = "/ucenter/address")
    public String userAddress(ModelMap modelMap){
        if (getUser() != null) {
            modelMap.addAttribute("user", getUser());
        }
        return theme.getPcTemplate("user/address");
    }

    //我的个人信息
    @GetMapping(value = "/ucenter/info")
    public String userInfo(ModelMap modelMap){
        if (getUser() != null) {
            modelMap.addAttribute("user", getUser());
        }
        return theme.getPcTemplate("user/info");
    }

    //我的密码修改
    @GetMapping(value = "/ucenter/password")
    public String userPassword(ModelMap modelMap){
        if (getUser() != null) {
            modelMap.addAttribute("user", getUser());
        }
        return theme.getPcTemplate("user/password");
    }

    //我的发票管理
    @GetMapping(value = "/ucenter/invoice")
    public String userInvoice(ModelMap modelMap){
        if (getUser() != null) {
            modelMap.addAttribute("user", getUser());
        }
        return theme.getPcTemplate("user/invoice");
    }




    /**
     * 用户退出登录
     *
     */
    @GetMapping(value = "/member/logout")
    public String logout() {
        UserSessionUtil.setLoginMember(request,null);
        return "redirect:/member/login";
    }
}
