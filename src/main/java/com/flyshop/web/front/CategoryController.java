package com.flyshop.web.front;

import com.flyshop.core.base.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * 开发公司：28844.com<br/>
 * 版权：28844.com<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 18:03 2018/7/8
 */
@Controller
public class CategoryController extends BaseController {

    //购物车
    @GetMapping(value = "/list/{id}")
    public String cart(@PathVariable(value = "id", required = false) String id, ModelMap modelMap){

        return theme.getPcTemplate("shopping/cart");
    }
}
