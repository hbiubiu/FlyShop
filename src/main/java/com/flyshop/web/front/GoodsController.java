package com.flyshop.web.front;

import com.flyshop.core.base.BaseController;
import com.flyshop.module.goods.model.Goods;
import com.flyshop.module.goods.service.GoodsService;
import com.flyshop.module.user.model.User;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class GoodsController extends BaseController {
    @Autowired
    private GoodsService goodsService;


    //产品详细页面
    @GetMapping(value = "/item/{id}")
    public String item(@PathVariable(value = "id", required = false) String id,ModelMap modelMap){
        if (!NumberUtils.isNumber(id)) {
            return theme.getPcTemplate("404");
        }
        Goods goods=goodsService.findGoodsByid(Integer.valueOf(id),1);
        if(goods==null){
            return theme.getPcTemplate("404");
        }
        if (getUser() != null) {
            modelMap.addAttribute("user", getUser());
        }
        modelMap.addAttribute("goods", goods);
        return theme.getPcTemplate("item");
    }

    //购物车
    @GetMapping(value = "/cart")
    public String cart(ModelMap modelMap){
        return theme.getPcTemplate("shopping/cart");
    }
}
