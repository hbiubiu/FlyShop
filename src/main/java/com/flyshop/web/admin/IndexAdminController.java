package com.flyshop.web.admin;

import com.flyshop.common.utils.AdminSessionUtil;
import com.flyshop.constant.Const;
import com.flyshop.core.base.BaseController;
import com.flyshop.core.entity.DataVo;
import com.flyshop.module.admin.model.Admin;
import com.flyshop.module.admin.service.AdminService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

/**
 * 开发公司：28844.com<br/>
 * 版权：28844.com<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: $time$ $date$
 */
@Controller
@RequestMapping("/admin/")
public class IndexAdminController extends BaseController {
    @Autowired
    protected AdminService adinsrv;

    /**
     * 首页
     *
     * @return
     */
    @GetMapping(value = "/index")
    public String index(ModelMap modelMap){
        modelMap.addAttribute("admin", getAdminUser());
        return theme.getAdminTemplate("index");
    }

    @GetMapping(value = "/login")
    public String adminLogin(@RequestParam(value = "redirectUrl",required = false) String redirectUrl,ModelMap modelMap){
        if(getUser() != null){
            return "redirect:/admin/index";
        }
        modelMap.addAttribute("redirectUrl",redirectUrl);
        return theme.getAdminTemplate("user/login");
    }

    @ResponseBody
    @PostMapping(value = "/login_act")
    public DataVo userLogin(
            @RequestParam(value = "admin_name", required = false) String admin_name,
            @RequestParam(value = "password", required = false) String password,
            @RequestParam(value = "redirectUrl",required = false) String redirectUrl,
            @RequestParam(value = "captcha", required = false) String captcha) {
        try {
            String kaptcha = (String) session.getAttribute("kaptcha");
            if (StringUtils.isBlank(admin_name)) {
                return DataVo.failure("用户名不能为空");
            }
            if (StringUtils.isBlank(password)) {
                return DataVo.failure("密码不能为空");
            } else if (password.length() < 6 && password.length() > 30) {
                return DataVo.failure("密码最少6个字符，最多30个字符");
            }
            // 校验验证码
            if (captcha != null) {
                if (!captcha.equalsIgnoreCase(kaptcha)) {
                    return DataVo.failure("验证码错误");
                }
            }else{
                return DataVo.failure("验证码不能为空");
            }
            Admin entity = adinsrv.adminLogin(admin_name,password,request);
            if(entity==null){
                return DataVo.failure("帐号或密码错误。");
            }else{
                session.removeAttribute(Const.KAPTCHA_SESSION_KEY);
                if (StringUtils.isNotEmpty(redirectUrl)){
                    return DataVo.jump("操作成功", redirectUrl);
                }
                return DataVo.jump("操作成功", "/admin/index");
            }
        } catch (Exception e) {
            return DataVo.failure("帐号或密码错误。");
        }
    }

    /**
     * 管理员退出登录
     *
     */
    @GetMapping(value = "/logout")
    public String logout() {
        AdminSessionUtil.setLoginMember(request,null);
        return "redirect:/admin/login";
    }
}
