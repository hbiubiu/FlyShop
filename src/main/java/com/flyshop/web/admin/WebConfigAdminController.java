package com.flyshop.web.admin;

import com.flyshop.core.base.BaseController;
import com.flyshop.module.webconfig.model.Areas;
import com.flyshop.module.webconfig.model.WebConfig;
import com.flyshop.module.webconfig.service.AreasService;
import com.flyshop.module.webconfig.service.WebConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 开发公司：28844.com<br/>
 * 版权：28844.com<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 2018-06-30
 */
@Controller
@RequestMapping("/admin/site")
public class WebConfigAdminController extends BaseController {
    @Autowired
    protected WebConfigService srv;

    @Autowired
    protected AreasService areasService;

    @GetMapping(value = "/basic")
    public String basic(ModelMap modelMap){
        Map<String,String> map=new HashMap<String,String>();
        List<WebConfig> configList=srv.getConfigAllList();
        for (WebConfig List : configList) {
            map.put(List.getKeycode(), List.getKeyvalue());
        }
        modelMap.addAttribute("config",map);
        modelMap.addAttribute("admin", getAdminUser());
        return theme.getAdminTemplate("webconfig/basic");
    }

    @GetMapping(value = "/area_list")
    public String getAreaList(@RequestParam(value = "parentId", defaultValue = "0") int parentId,ModelMap modelMap){
        List<Areas> areas=areasService.selectAreasByPid(parentId);
        modelMap.addAttribute("areas",areas);
        modelMap.addAttribute("admin", getAdminUser());
        return theme.getAdminTemplate("webconfig/area_list");
    }

    @GetMapping(value = "/oauth_list")
    public String getOauthList(ModelMap modelMap){
        modelMap.addAttribute("admin", getAdminUser());
        return theme.getAdminTemplate("webconfig/oauth_list");
    }

    @GetMapping(value = "/conf_guide")
    public String getAddGuide(ModelMap modelMap){
        modelMap.addAttribute("admin", getAdminUser());
        return theme.getAdminTemplate("webconfig/conf_guide");
    }
}
