package com.flyshop.web.admin;

import com.flyshop.core.base.BaseController;
import com.flyshop.core.entity.DataVo;
import com.flyshop.core.entity.PageVo;
import com.flyshop.module.admin.model.Admin;
import com.flyshop.module.admin.service.AdminService;
import com.flyshop.module.security.model.Role;
import com.flyshop.module.security.service.RoleService;
import com.flyshop.module.user.model.UserGroup;
import com.flyshop.module.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 开发公司：28844.com<br/>
 * 版权：28844.com<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 2018-7-4
 */
@Controller
@RequestMapping("/admin/user")
public class AdminController extends BaseController {

    @Autowired
    protected AdminService adminService;

    @Autowired
    protected RoleService roleService;

    //用户列表
    @GetMapping(value = "/admin_list")
    public String adminList(@RequestParam(value = "adminName", required = false) String adminName,
                            @RequestParam(value = "nickName", required = false) String nickName,
                            @RequestParam(value = "mobile", required = false) String mobile,
                            @RequestParam(value = "email", required = false) String email,
                            @RequestParam(value = "p", defaultValue = "1") int pageNum,
                            ModelMap modelMap){
        PageVo<Admin> pageVo=adminService.getAdminListPage(adminName, nickName, mobile, email,pageNum,20);
        List<Role> role=roleService.getAllRoleList();
        modelMap.put("role", role);
        modelMap.put("pageVo", pageVo);
        modelMap.addAttribute("admin", getAdminUser());
        return theme.getAdminTemplate("webconfig/admin_list");
    }

    //添加管理员
    @GetMapping(value = "/admin_add")
    public String adminAdd(ModelMap modelMap){
        List<Role> role=roleService.getAllRoleList();
        modelMap.put("role", role);
        modelMap.addAttribute("admin", getAdminUser());
        return theme.getAdminTemplate("webconfig/admin_add");
    }

    //处理和保存管理员信息
    @PostMapping("/admin_save")
    @ResponseBody
    public DataVo addAdminSave(@Valid Admin admin, BindingResult result){
        DataVo data = DataVo.failure("操作失败");
        try {
            if (result.hasErrors()) {
                List<ObjectError> list = result.getAllErrors();
                for (ObjectError error : list) {
                    return DataVo.failure(error.getDefaultMessage());
                }
                return null;
            }
            data = adminService.addAdmin(admin);
        } catch (Exception e) {
            data = DataVo.failure(e.getMessage());
        }
        return data;
    }

    //编辑用户
    @GetMapping(value = "/admin_edit/{adminId}")
    public String adminEdit(@PathVariable(value = "adminId", required = false) int adminId,ModelMap modelMap){
        Admin user=adminService.findAdminById(adminId,0);
        if(user==null){
            return theme.getAdminTemplate("404");
        }
        int roleId=roleService.findAdminAndRoleById(adminId);
        List<Role> role=roleService.getAllRoleList();
        modelMap.put("roleId", roleId);
        modelMap.put("role", role);
        modelMap.addAttribute("admin", getAdminUser());
        modelMap.addAttribute("user", user);
        return theme.getAdminTemplate("webconfig/admin_edit");
    }

    //处理和保存更新后管理员信息
    @PostMapping("/admin_act")
    @ResponseBody
    public DataVo addAdminAct(@Valid Admin admin, BindingResult result){
        DataVo data = DataVo.failure("操作失败");
        try {
            if (result.hasErrors()) {
                List<ObjectError> list = result.getAllErrors();
                for (ObjectError error : list) {
                    return DataVo.failure(error.getDefaultMessage());
                }
                return null;
            }
            data = adminService.updateAdmin(admin);
        } catch (Exception e) {
            data = DataVo.failure(e.getMessage());
        }
        return data;
    }

    //删除管理员
    @PostMapping("/delAdmin")
    @ResponseBody
    public DataVo deleteAdminById(@RequestParam(value = "id") int id){
        DataVo data = DataVo.failure("操作失败");
        if(id==1){
            return data = DataVo.failure("超级管理员组不能删除");
        }
        data = adminService.deleteAdminById(id);
        return data;
    }
}
