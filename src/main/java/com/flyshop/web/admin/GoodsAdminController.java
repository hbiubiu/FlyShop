package com.flyshop.web.admin;

import com.flyshop.core.base.BaseController;
import com.flyshop.core.entity.DataVo;
import com.flyshop.core.entity.PageVo;
import com.flyshop.module.goods.model.Category;
import com.flyshop.module.goods.model.Goods;
import com.flyshop.module.goods.service.CategoryService;
import com.flyshop.module.goods.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 开发公司：28844.com<br/>
 * 版权：28844.com<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 2018-7-2
 */
@Controller
@RequestMapping("/admin/goods")
public class GoodsAdminController  extends BaseController {
    @Autowired
    protected GoodsService goodsService;

    @Autowired
    protected CategoryService csrv;

    @GetMapping(value = "/goods_list")
    public String roleList(@RequestParam(value = "p", defaultValue = "1") int pageNum, ModelMap modelMap){
        modelMap.addAttribute("admin", getAdminUser());
        return theme.getAdminTemplate("goods/goods_list");
    }

    //添加产品
    @GetMapping(value = "/goods_add")
    public String addGoods(ModelMap modelMap){
        modelMap.addAttribute("admin", getAdminUser());
        return theme.getAdminTemplate("goods/goods_add");
    }

    //添加产品
    @GetMapping(value = "/goods_edit/{id}")
    public String editGoods(@PathVariable int id,ModelMap modelMap){
        Goods goods=goodsService.findGoodsByid(id,0);
        modelMap.addAttribute("admin", getAdminUser());
        modelMap.addAttribute("goods", goods);
        return theme.getAdminTemplate("goods/goods_edit");
    }

    //产品分类列表
    @GetMapping(value = "/category_list")
    public String categoryList(ModelMap modelMap){
        modelMap.addAttribute("admin", getAdminUser());
        return theme.getAdminTemplate("goods/category_list");
    }

    //添加列表
    @GetMapping(value = "/add_category")
    public String addCategory(ModelMap modelMap){
        modelMap.addAttribute("admin", getAdminUser());
        return theme.getAdminTemplate("goods/category_add");
    }

    @ResponseBody
    @PostMapping(value = "/category_save")
    public DataVo addUser(@Valid Category category, BindingResult result) {
        DataVo data = DataVo.failure("操作失败");
        try {
            if (result.hasErrors()) {
                List<ObjectError> list = result.getAllErrors();
                for (ObjectError error : list) {
                    return DataVo.failure(error.getDefaultMessage());
                }
                return null;
            }
            data = csrv.addCategory(category);
        } catch (Exception e) {
            data = DataVo.failure(e.getMessage());
        }
        return data;
    }

    //修改列表
    @GetMapping(value = "/edit_category")
    public String editCategory(ModelMap modelMap){
        modelMap.addAttribute("admin", getAdminUser());
        return theme.getAdminTemplate("goods/category_edit");
    }

}
