package com.flyshop.web.admin;

import com.flyshop.core.base.BaseController;
import com.flyshop.core.entity.DataVo;
import com.flyshop.core.entity.PageVo;
import com.flyshop.module.article.model.Article;
import com.flyshop.module.article.model.ArticleCategory;
import com.flyshop.module.article.service.ArticleCategoryService;
import com.flyshop.module.article.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 开发公司：28844.com<br/>
 * 版权：28844.com<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 10:57 2018/7/13
 */
@Controller
@RequestMapping("/admin/tools")
public class ArticleAdminController extends BaseController {
    @Autowired
    protected ArticleService articleService;

    @Autowired
    protected ArticleCategoryService articleCategoryService;

    //分类列表
    @GetMapping(value = "/article_category_list")
    public String getAddArticleCategoryList(@RequestParam(value = "p", defaultValue = "1") int pageNum,ModelMap modelMap){
        PageVo<ArticleCategory> pageVo=articleCategoryService.getArticleCategoryListPage(pageNum,20);
        modelMap.addAttribute("pageVo", pageVo);
        modelMap.addAttribute("admin", getAdminUser());
        return theme.getAdminTemplate("tools/article_category_list");
    }

    @GetMapping(value = "/article_category_add")
    public String getAddArticleCategoryAdd(ModelMap modelMap){
        modelMap.addAttribute("admin", getAdminUser());
        return theme.getAdminTemplate("tools/article_category_add");
    }

    //保存添加文章分类
    @PostMapping("/article_category_save")
    @ResponseBody
    public DataVo addAddArticleCategorySave(@Valid ArticleCategory articleCategory, BindingResult result){
        DataVo data = DataVo.failure("操作失败");
        try {
            if (result.hasErrors()) {
                List<ObjectError> list = result.getAllErrors();
                for (ObjectError error : list) {
                    return DataVo.failure(error.getDefaultMessage());
                }
                return null;
            }
            data = articleCategoryService.addArticleCategory(articleCategory);
        } catch (Exception e) {
            data = DataVo.failure(e.getMessage());
        }
        return data;
    }

    @GetMapping("/article_category/father")
    @ResponseBody
    public List<ArticleCategory> getArticleCategoryListByFatherId(@RequestParam(value = "parentId", defaultValue = "0") Integer parentId){
        List<ArticleCategory> list=articleCategoryService.getArticleCategoryListByFatherId(parentId);
        return list;
    }


    @GetMapping(value = "/article_category_edit")
    public String getAddArticleCategoryEdit(ModelMap modelMap){
        modelMap.addAttribute("admin", getAdminUser());
        return theme.getAdminTemplate("tools/article_category_edit");
    }

    //文章列表
    @GetMapping(value = "/article_list")
    public String getArticleList(@RequestParam(value = "title", required = false) String title,
                                 @RequestParam(value = "createTime", required = false) String createTime,
                                 @RequestParam(value = "p", defaultValue = "1") int pageNum,
                                 ModelMap modelMap){
        PageVo<Article> pageVo=articleService.getArticleListPage(title,createTime,pageNum, 20);
        modelMap.addAttribute("pageVo", pageVo);
        modelMap.addAttribute("admin", getAdminUser());
        return theme.getAdminTemplate("tools/article_list");
    }

    //添加文章
    @GetMapping(value = "/article_add")
    public String getAddArticle(ModelMap modelMap){
        modelMap.addAttribute("admin", getAdminUser());
        return theme.getAdminTemplate("tools/article_add");
    }

    //保存添加文章
    @PostMapping("/article_save")
    @ResponseBody
    public DataVo addAdminSave(@Valid Article article, BindingResult result){
        DataVo data = DataVo.failure("操作失败");
        try {
            if (result.hasErrors()) {
                List<ObjectError> list = result.getAllErrors();
                for (ObjectError error : list) {
                    return DataVo.failure(error.getDefaultMessage());
                }
                return null;
            }
            data = articleService.addArticle(article);
        } catch (Exception e) {
            data = DataVo.failure(e.getMessage());
        }
        return data;
    }
}
