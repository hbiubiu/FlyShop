/*
 *	Copyright © 2015 Zhejiang SKT Science Technology Development Co., Ltd. All rights reserved.
 *	浙江斯凯特科技发展有限公司 版权所有
 *	http://www.28844.com
 *
 */
package com.flyshop.web.tags;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.flyshop.core.base.AbstractTagPlugin;
import com.flyshop.module.user.model.UserGroup;
import com.flyshop.module.user.service.UserGroupService;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import freemarker.core.Environment;
import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapperBuilder;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;

/**
 * 查询用户组标签
 * 
 * @author sunkaifei
 * 
 */
@Service
public class Usergroup extends AbstractTagPlugin {
	@Autowired
	private UserGroupService userGroupService;
		
	@Override
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void execute(Environment env, Map params, TemplateModel[] loopVars,
			TemplateDirectiveBody body) throws TemplateException, IOException {
		DefaultObjectWrapperBuilder builder = new DefaultObjectWrapperBuilder(Configuration.VERSION_2_3_25);
		// 获取页面的参数
		int id=0;
		//处理标签变量
		Map<String, TemplateModel> paramWrap = new HashMap<String, TemplateModel>(params);
		for(String str:paramWrap.keySet()){
			if("id".equals(str)){
				if (!NumberUtils.isNumber(paramWrap.get(str).toString())) {
					id = 0;
				}else{
					id = Integer.parseInt(paramWrap.get(str).toString());
				}
			}
		}
		// 获取文章所有信息
		try {
			UserGroup group = userGroupService.findUserGroupByid(id);
			if(group!=null){
				env.getOut().write(group.getGroupName());
			}else{
				env.getOut().write("");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
