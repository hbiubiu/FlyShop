/*
 *	Copyright © 2015 Zhejiang SKT Science Technology Development Co., Ltd. All rights reserved.
 *	浙江斯凯特科技发展有限公司 版权所有
 *	http://www.28844.com
 *
 */

package com.flyshop.web.tags;

import com.flyshop.constant.Const;
import com.flyshop.core.base.AbstractTagPlugin;
import com.flyshop.module.template.service.TemplateService;
import com.flyshop.module.user.model.User;
import freemarker.core.Environment;
import freemarker.template.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Map;

/**
 * 网站样式等静态资源路径查询标签
 * 
 * @author sunkaifei
 * 
 */
@Service
public class Login extends AbstractTagPlugin {

	@Override
	@SuppressWarnings("rawtypes")
	public void execute(Environment env, Map params, TemplateModel[] loopVars, TemplateDirectiveBody body) throws TemplateException, IOException {
		DefaultObjectWrapperBuilder builder = new DefaultObjectWrapperBuilder(Configuration.VERSION_2_3_25);
		try {

			//获取HttpSession
			ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
			HttpServletRequest request = attr.getRequest();
			User loginMember = (User) request.getSession().getAttribute(Const.SESSION_USER);
			env.setVariable("status", builder.build().wrap(loginMember));
			body.render(env.getOut());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
