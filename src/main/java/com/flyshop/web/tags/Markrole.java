/*
 *	Copyright © 2015 Zhejiang SKT Science Technology Development Co., Ltd. All rights reserved.
 *	浙江斯凯特科技发展有限公司 版权所有
 *	http://www.28844.com
 *
 */

package com.flyshop.web.tags;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.flyshop.core.base.AbstractTagPlugin;
import com.flyshop.module.security.service.PermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import freemarker.core.Environment;
import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapperBuilder;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;

/**
 * 权限组权限查询查询标签
 * 
 * @author sunkaifei
 * 
 */
@Service
public class Markrole extends AbstractTagPlugin {
	
	@Autowired
	private PermissionService srv;

	@Override
	@SuppressWarnings("rawtypes")
	public void execute(Environment env, Map params, TemplateModel[] loopVars,
			TemplateDirectiveBody body) throws TemplateException, IOException {
		DefaultObjectWrapperBuilder builder = new DefaultObjectWrapperBuilder(Configuration.VERSION_2_3_25);
		try {
			// 获取页面的参数
			Integer roleId = 0;
			// 获取文件的分页
			//审核设置，默认0
			Integer permissionId = 0;
			//处理标签变量
			@SuppressWarnings("unchecked")
			Map<String, TemplateModel> paramWrap = new HashMap<String, TemplateModel>(params);
			for(String str:paramWrap.keySet()){ 
				if("roleId".equals(str)){
					roleId = Integer.parseInt(paramWrap.get(str).toString());
				}
				if("permissionId".equals(str)){
					permissionId = Integer.parseInt(paramWrap.get(str).toString());
				}

			}
			boolean mark = srv.markAssignedPermissions(roleId,permissionId);
			env.setVariable("mark", builder.build().wrap(mark));
			body.render(env.getOut());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
