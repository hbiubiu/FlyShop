/*
 *	Copyright © 2015 Zhejiang SKT Science Technology Development Co., Ltd. All rights reserved.
 *	浙江斯凯特科技发展有限公司 版权所有
 *	http://www.28844.com
 *
 */

package com.flyshop.web.tags;

import com.flyshop.core.base.AbstractTagPlugin;
import com.flyshop.module.template.service.TemplateService;
import freemarker.core.Environment;
import freemarker.template.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * 网站样式等静态资源路径查询标签
 * 
 * @author sunkaifei
 * 
 */
@Service
public class Site extends AbstractTagPlugin {
	
	@Autowired
	private TemplateService srv;

	@Override
	@SuppressWarnings("rawtypes")
	public void execute(Environment env, Map params, TemplateModel[] loopVars, TemplateDirectiveBody body) throws TemplateException, IOException {
		try {
			String key = null;
			//处理标签变量
			if("pcSkin".equals(params.get("key").toString())){
				key = srv.getPcSkin().toString();
			}
			if("mSkin".equals(params.get("key").toString())){
				key = srv.getMobileSkin().toString();
			}
			env.getOut().write(key);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
