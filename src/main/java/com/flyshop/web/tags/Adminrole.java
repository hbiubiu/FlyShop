/*
 *	Copyright © 2015 Zhejiang SKT Science Technology Development Co., Ltd. All rights reserved.
 *	浙江斯凯特科技发展有限公司 版权所有
 *	http://www.28844.com
 *
 */
package com.flyshop.web.tags;

import com.flyshop.core.base.AbstractTagPlugin;
import com.flyshop.module.security.model.Role;
import com.flyshop.module.security.service.RoleService;
import freemarker.core.Environment;
import freemarker.template.*;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * 查询管理员所在权限组标签
 * 
 * @author sunkaifei
 * 
 */
@Service
public class Adminrole extends AbstractTagPlugin {
	@Autowired
	protected RoleService srv;
		
	@Override
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void execute(Environment env, Map params, TemplateModel[] loopVars,
			TemplateDirectiveBody body) throws TemplateException, IOException {
		DefaultObjectWrapperBuilder builder = new DefaultObjectWrapperBuilder(Configuration.VERSION_2_3_25);
		// 获取页面的参数
		int id=0;
		//处理标签变量
		Map<String, TemplateModel> paramWrap = new HashMap<String, TemplateModel>(params);
		for(String str:paramWrap.keySet()){
			if("id".equals(str)){
				if (!NumberUtils.isNumber(paramWrap.get(str).toString())) {
					id = 0;
				}else{
					id = Integer.parseInt(paramWrap.get(str).toString());
				}
			}
		}
		// 按管理员id查询所在会员组信息
		try {
			Role role = srv.findAdminByRole(id);
			if(role!=null){
				env.getOut().write(role.getName());
			}else{
				env.getOut().write("");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
