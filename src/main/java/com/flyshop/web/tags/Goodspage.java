/*
 *	Copyright © 2015 Zhejiang SKT Science Technology Development Co., Ltd. All rights reserved.
 *	浙江斯凯特科技发展有限公司 版权所有
 *	http://www.28844.com
 *
 */

package com.flyshop.web.tags;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.flyshop.core.base.AbstractTagPlugin;
import com.flyshop.core.entity.PageVo;
import com.flyshop.module.goods.model.Goods;
import com.flyshop.module.goods.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import freemarker.core.Environment;
import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapperBuilder;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;

/**
 * @author sunkaifei
 * 
 */
@Service
public class Goodspage extends AbstractTagPlugin {

	@Autowired
	private GoodsService goodsService;

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void execute(Environment env, Map params, TemplateModel[] loopVars,
			TemplateDirectiveBody body) throws TemplateException, IOException {
		DefaultObjectWrapperBuilder builder = new DefaultObjectWrapperBuilder(Configuration.VERSION_2_3_25);
		// 获取页面的参数
		//所属主信息类型，0是所有，1是文章，2是小组话题
		String name = null;

		Integer storeNums = null;

		Integer sellerId = null;

		Integer brandId = null;
		
		String upTime=null;

		String downTime=null;
		
		Integer isDel=null;
		//翻页页数
		Integer p = 1;
		//每页记录条数
		Integer rows = 10;
		//处理标签变量
		Map<String, TemplateModel> paramWrap = new HashMap<String, TemplateModel>(params);
		for(String str:paramWrap.keySet()){ 
			if("name".equals(str)){
				name = paramWrap.get(str).toString();
			}
			if("storeNums".equals(str)){
				storeNums = Integer.parseInt(paramWrap.get(str).toString());
			}
			if("sellerId".equals(str)){
				sellerId = Integer.parseInt(paramWrap.get(str).toString());
			}
			if("brandId".equals(str)){
				brandId = Integer.parseInt(paramWrap.get(str).toString());
			}
			if("upTime".equals(str)){
				upTime = paramWrap.get(str).toString();
			}
			if("downTime".equals(str)){
				downTime = paramWrap.get(str).toString();
			}
			if("isDel".equals(str)){
				isDel = Integer.parseInt(paramWrap.get(str).toString());
			}
			if("p".equals(str)){
				p = Integer.parseInt(paramWrap.get(str).toString());
			}
			if("rows".equals(str)){
				rows = Integer.parseInt(paramWrap.get(str).toString());
			}
		}
		// 获取文件的分页
		try {
			PageVo<Goods> pageVo = goodsService.getGoodsListPage(name, storeNums, sellerId, brandId, upTime, downTime,isDel,p,rows);
			env.setVariable("goods_page", builder.build().wrap(pageVo));
		} catch (Exception e) {
			env.setVariable("goods_page", builder.build().wrap(null));
		}
		body.render(env.getOut());
	}
}
