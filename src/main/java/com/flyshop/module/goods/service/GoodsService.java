package com.flyshop.module.goods.service;

import com.flyshop.core.entity.PageVo;
import com.flyshop.module.goods.dao.GoodsDao;
import com.flyshop.module.goods.model.Goods;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class GoodsService {
    @Autowired
    protected GoodsDao dao;
    // ///////////////////////////////
    // /////       增加       ////////
    // ///////////////////////////////

    // ///////////////////////////////
    // /////        刪除      ////////
    // ///////////////////////////////

    // ///////////////////////////////
    // /////        修改      ////////
    // ///////////////////////////////


    // ///////////////////////////////
    // /////        查询      ////////
    // ///////////////////////////////
    /**
     * 按id查询产品信息
     *
     * @param id
     *         产品id
     * @param isDel
     *         0所有，1正常 2已删除 3下架 4申请上架
     * @return
     */
    public Goods findGoodsByid(int id,int isDel){
        return dao.findGoodsByid(id,isDel);
    }

    /**
     * 商品翻页查询
     *
     * @param pageNum
     * @param rows
     * @return
     * @throws Exception
     */
    public PageVo<Goods> getGoodsListPage(String name,Integer storeNums,Integer sellerId,Integer brandId,String upTime,String downTime,Integer isDel,int pageNum, int rows) {
        PageVo<Goods> pageVo = new PageVo<Goods>(pageNum);
        pageVo.setRows(rows);
        List<Goods> list = new ArrayList<Goods>();
        pageVo.setList(dao.getGoodsList(name, storeNums, sellerId, brandId, upTime, downTime,isDel,pageVo.getOffset(), pageVo.getRows()));
        pageVo.setCount(dao.getGoodsCount(name, storeNums, sellerId, brandId, upTime, downTime,isDel));
        return pageVo;
    }
}
