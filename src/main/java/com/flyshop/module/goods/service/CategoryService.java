package com.flyshop.module.goods.service;

import com.flyshop.core.entity.DataVo;
import com.flyshop.module.goods.dao.CategoryDao;
import com.flyshop.module.goods.model.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 开发公司：28844.com<br/>
 * 版权：28844.com<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 2018-7-3
 */
@Service
public class CategoryService {
    @Autowired
    protected CategoryDao dao;
    // ///////////////////////////////
    // /////      增加        ////////
    // ///////////////////////////////
    //添加产品分类信息
    public DataVo addCategory(Category category){
        DataVo data = DataVo.failure("操作失败");
        if(this.checkCategory(category.getName())){
            data=DataVo.failure("该分类名已重复");
        }else{
            dao.addCategory(category);
            data=DataVo.success("已添加成功");
        }
        return data;
    }

    // ///////////////////////////////
    // /////        刪除      ////////
    // ///////////////////////////////

    // ///////////////////////////////
    // /////        修改      ////////
    // ///////////////////////////////

    // ///////////////////////////////
    // /////       查询       ////////
    // ///////////////////////////////
    public boolean checkCategory(String name) {
        int totalCount = dao.checkCategory(name);
        return totalCount > 0 ? true : false;
    }

}
