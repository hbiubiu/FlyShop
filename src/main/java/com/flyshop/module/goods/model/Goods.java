package com.flyshop.module.goods.model;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * 开发公司：28844.com<br/>
 * 版权：28844.com<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 2018-7-2
 */
@Setter
@Getter
public class Goods implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer id;
    private String name;
    private String goodsNo;
    private Integer modelId;
    private String sellPrice;
    private String marketPrice;
    private String costPrice;
    private Date upTime;
    private Date downTime;
    private Date createTime;
    private String storeNums;
    private String img;
    private String adImg;
    //商品状态 0正常 1已删除 2下架 3申请上架
    private String isDel;
    private String content;
    private String keywords;
    private String description;
    private String searchWords;
    private String weight;
    private Integer point;
    private String unit;
    private Integer brandId;
    private Integer visit;
    private Integer favorite;
    private Integer sort;
    private String specArray;
    private Integer exp;
    private Integer comments;
    private Integer sale;
    private Integer grade;
    private Integer sellerId;
    private Integer isShare;
    private Integer isDeliveryFee;
}
