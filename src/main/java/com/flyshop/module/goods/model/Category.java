package com.flyshop.module.goods.model;

import lombok.Getter;
import lombok.Setter;
import javax.validation.constraints.NotEmpty;

import java.io.Serializable;
import java.util.Date;

/**
 * 开发公司：28844.com<br/>
 * 版权：28844.com<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 2018-7-3
 */
@Setter
@Getter
public class Category implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer id;
    @NotEmpty(message="分类名不能为空")
    private String name;
    private Integer parent_id;
    private Integer sort;
    private String visibility;
    private String keywords;
    private String descript;
    private String title;
}
