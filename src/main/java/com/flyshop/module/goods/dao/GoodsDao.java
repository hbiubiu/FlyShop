package com.flyshop.module.goods.dao;

import com.flyshop.module.goods.model.Goods;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 开发公司：28844.com<br/>
 * 版权：28844.com<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: $time$ $date$
 */
@Repository
public interface GoodsDao {

    // ///////////////////////////////
    // /////       增加       ////////
    // ///////////////////////////////

    // ///////////////////////////////
    // /////        刪除      ////////
    // ///////////////////////////////

    // ///////////////////////////////
    // /////        修改      ////////
    // ///////////////////////////////



    // ///////////////////////////////
    // /////        查詢      ////////
    // ///////////////////////////////

    /**
     * 按id查询产品信息
     *
     * @param id
     *         产品id
     * @param isDel
     *         0所有，1正常 2已删除 3下架 4申请上架
     * @return
     */
    public Goods findGoodsByid(@Param("id") int id,@Param("isDel") int isDel);

    //查询所有商品数量
    public int getGoodsCount(@Param("name") String name,
                             @Param("storeNums") Integer storeNums,
                             @Param("sellerId") Integer sellerId,
                             @Param("brandId") Integer brandId,
                             @Param("upTime") String upTime,
                             @Param("downTime") String downTime,
                             @Param("isDel") Integer isDel);

    //商品列表
    public List<Goods> getGoodsList(@Param("name") String name,
                                    @Param("storeNums") Integer storeNums,
                                    @Param("sellerId") Integer sellerId,
                                    @Param("brandId") Integer brandId,
                                    @Param("upTime") String upTime,
                                    @Param("downTime") String downTime,
                                    @Param("isDel") Integer isDel,
                                    @Param("offset") Integer offset,
                                    @Param("rows") Integer rows);

}
