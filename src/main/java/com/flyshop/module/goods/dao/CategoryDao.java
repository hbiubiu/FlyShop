package com.flyshop.module.goods.dao;

import com.flyshop.module.goods.model.Category;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * 开发公司：28844.com<br/>
 * 版权：28844.com<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 2018-7-3
 */
@Repository
public interface CategoryDao {
    // ///////////////////////////////
    // /////      增加        ////////
    // ///////////////////////////////
    //添加分类信息
    public int addCategory(Category category);

    // ///////////////////////////////
    // ///// 查詢 ////////
    // ///////////////////////////////
    //检查是否有分类名重复
    public int checkCategory(@Param("name") String name);
}
