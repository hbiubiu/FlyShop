package com.flyshop.module.security.service;

import com.flyshop.core.entity.DataVo;
import com.flyshop.core.entity.PageVo;
import com.flyshop.module.security.dao.RoleDao;
import com.flyshop.module.security.model.Role;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 开发公司：28844.com<br/>
 * 版权：28844.com<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: $time$ $date$
 */
@Service
public class RoleService {
    @Autowired
    private RoleDao dao;

    // ///////////////////////////////
    // /////      增加        ////////
    // ///////////////////////////////
    //添加用户组名
    public DataVo addRole(String name){
        if(this.checkRole(name)){
            return DataVo.failure("该用户组名已存在");
        }
        Role role=new Role();
        role.setName(name);
        role.setCreateAt(new Date());
        dao.addRole(role);
        return DataVo.success("添加成功！");
    }

    public boolean addRolePermission(int roleId,int permissionId){
        int totalCount = dao.addRolePermission(roleId,permissionId);
        return totalCount > 0 ? true : false;
    }
    // ///////////////////////////////
    // /////        删除      ////////
    // ///////////////////////////////
    //按id删除角色信息
    public boolean deleteRole(int id){
        dao.deleteRolePermission(id,null);
        int totalCount = dao.deleteRole(id);
        return totalCount > 0 ? true : false;
    }

    //按id删除角色和权限关联信息
    public boolean deleteRolePermission(int roleId,int permissionId){
        int totalCount = dao.deleteRolePermission(roleId,permissionId);
        return totalCount > 0 ? true : false;
    }

    // ///////////////////////////////
    // /////       修改       ////////
    // ///////////////////////////////
    //按id修改组名
    public DataVo updateRole(String name, int id){
        DataVo data = DataVo.failure("操作失败");
        if(dao.updateRole(name,id)>0){
            data=DataVo.success("修改成功");
        }else{
            data=DataVo.failure("修改失败！");
        }
        return data;
    }

    // ///////////////////////////////
    // /////       查询       ////////
    // ///////////////////////////////
    //按id查询权限组信息
    public Role findRoleById(int id){
        return dao.findRoleById(id);
    }

    //检查该权限是否存在
    public boolean checkRole(String name) {
        int totalCount = dao.checkRole(name);
        return totalCount > 0 ? true : false;
    }

    /**
     * 用户组翻页查询
     *
     * @param pageNum
     * @param rows
     * @return
     * @throws Exception
     */
    public PageVo<Role> getRoleListPage(int pageNum, int rows) {
        PageVo<Role> pageVo = new PageVo<Role>(pageNum);
        pageVo.setRows(rows);
        List<Role> list = new ArrayList<Role>();
        pageVo.setList(dao.getRoleList(pageVo.getOffset(), pageVo.getRows()));
        pageVo.setCount(dao.getRoleCount());
        return pageVo;
    }

    //所有权限小组列表
    public List<Role> getAllRoleList(){
        return dao.getAllRoleList();
    };

    //按管理员id查询所在会员组id
    public Integer findAdminAndRoleById(int adminId){
        return  dao.findAdminAndRoleById(adminId);
    }

    //按管理员id查询所在会员组信息
    public Role findAdminByRole(int adminId){
        return  dao.findAdminByRole(adminId);
    }
}
