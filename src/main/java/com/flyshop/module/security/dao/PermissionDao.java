package com.flyshop.module.security.dao;

import com.flyshop.module.security.model.Permission;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PermissionDao {

    // ///////////////////////////////
    // /////      增加        ////////
    // ///////////////////////////////
    /**
     * @author: kaifei-sun
     * @Description:
     * @param: pre
     * @Date: 11:47 2018/6/28
     *
     */
    public int addPermission(Permission pre);

    // ///////////////////////////////
    // /////        删除      ////////
    // ///////////////////////////////
    //按id删除权限
    public int deletePermission(@Param("id") int id);

    //按id删除权限和权限组关联信息
    public int deleteRolePermission(@Param("permissionId") int permissionId);

    // ///////////////////////////////
    // /////        修改      ////////
    // ///////////////////////////////
    public int updatePermissions(Permission pre);

    // ///////////////////////////////
    // /////       查詢       ////////
    // ///////////////////////////////
    //按id查询权限节点详细信息
    public Permission findPermissionById(@Param("id") int id);

    //查询权限url是否存在
    public int checkPermission(@Param("actionKey") String actionKey,@Param("controller")  String controller);

    /**
     * @author: kaifei-sun
     * @param: id
     *          用户id
     * @Description: 查询当前用户所有权限
     * @Date: 11:58 2018/6/28
     *
     */
    public List<Permission> findPermissionByUserId(@Param("adminId") int adminId);

    //查询所有权限数量
    public int getPermissionCount();

    //权限列表
    public List<Permission> getPermissionList(@Param("offset") int offset,@Param("rows") int rows);

    //查询所有权限list
    public List<Permission> getAllPermissions();

    //检查是否有权限
    public int markAssignedPermissions(@Param("roleId") int roleId,@Param("permissionId") int permissionId);
}
