package com.flyshop.module.order.model;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
@Setter
@Getter
public class Order implements Serializable {
    private static final long serialVersionUID = 1L;
    private String orderNo;
    private Integer userId;
    private Integer payType;
    private Integer distribution;
    private Boolean status;
    private Boolean payStatus;
    private Boolean distributionStatus;
    private String acceptName;
    private String postcode;
    private String telphone;
    private Integer country;
    private Integer province;
    private Integer city;
    private Integer area;
    private String address;
    private String mobile;
    private BigDecimal payableAmount;
    private BigDecimal realAmount;
    private BigDecimal payableFreight;
    private BigDecimal realFreight;
    private Date payTime;
    private Date sendTime;
    private Date createTime;
    private Date completionTime;
    private Boolean invoice;
    private String postscript;
    private String note;
    private Boolean ifDel;
    private BigDecimal insured;
    private BigDecimal payFee;
    private String invoiceInfo;
    private BigDecimal taxes;
    private BigDecimal promotions;
    private BigDecimal discount;
    private BigDecimal orderAmount;
    private String prop;
    private String acceptTime;
    private Short exp;
    private Short point;
    private Boolean type;
    private String tradeNo;
    private Integer takeself;
    private String checkcode;
    private Integer activeId;
    private Integer sellerId;
    private Boolean isCheckout;
    private String proruleIds;
    private Integer spendPoint;
}