package com.flyshop.module.brand.service;

import com.flyshop.core.entity.DataVo;
import com.flyshop.module.brand.dao.BrandDao;
import com.flyshop.module.brand.model.BrandCategory;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

/**
 * 开发公司：28844.com<br/>
 * 版权：28844.com<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 10:54 2018/7/13
 */
@Service
public class BrandService {
    @Autowired
    protected BrandDao dao;

    // ///////////////////////////////
    // /////      增加        ////////
    // ///////////////////////////////
    //添加品牌分类
    public DataVo addBrandCategory(BrandCategory category){
        DataVo data = DataVo.failure("操作失败");
        if(this.checkCategoryByName(category.getName())){
            return data=DataVo.failure("分类已存在！");
        }
        int totalCount=dao.addBrandCategory(category);
        if(totalCount > 0){
            data = DataVo.success("分类添加成功！");
        }else{
            data=DataVo.failure("添加失败！");
        }
        return data;
    }
    // ///////////////////////////////
    // /////        刪除      ////////
    // ///////////////////////////////
    //按id删除用户信息
    @Transactional
    public DataVo deleteCategoryById(int id){
        DataVo data = DataVo.failure("操作失败");
        int totalCount = dao.deleteCategoryById(id);
        if(totalCount > 0){
            data = DataVo.success("该品牌分类已删除");
        }else{
            data=DataVo.failure("删除失败，请联系管理员！");
        }
        return data;
    }

    // ///////////////////////////////
    // /////        修改      ////////
    // ///////////////////////////////
    //按id更新品牌分类信息
    @Transactional
    public DataVo updateCategoryById(BrandCategory category){
        DataVo data = DataVo.failure("操作失败");
        int totalCount = dao.updateCategoryById(category);
        if(totalCount > 0){
            data = DataVo.success("成功更新品牌分类");
        }else{
            data=DataVo.failure("更新失败，信息不存在或者参数错误！");
        }
        return data;
    }


    // ///////////////////////////////
    // /////       查询       ////////
    // ///////////////////////////////

    //检查文章标题是否存在
    public boolean checkCategoryByName(String name) {
        int totalCount = dao.checkCategoryByName(name);
        return totalCount > 0 ? true : false;
    }

    //按id查询权限组信息
    public BrandCategory findCategoryById(int id){
        return dao.findCategoryById(id);
    }

    //所有品牌分类列表
    public List<BrandCategory> getAllCategoryList(){
        return dao.getAllCategoryList();
    };
}
