package com.flyshop.module.brand.dao;

import com.flyshop.module.brand.model.BrandCategory;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 开发公司：28844.com<br/>
 * 版权：28844.com<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 10:51 2018/7/13
 */
@Repository
public interface BrandDao {
    // ///////////////////////////////
    // /////       增加       ////////
    // ///////////////////////////////
    //添加文章
    public int addBrandCategory(BrandCategory brandCategory);


    // ///////////////////////////////
    // /////        刪除      ////////
    // ///////////////////////////////
    //按id删除品牌分类信息
    public int deleteCategoryById(@Param("id") int id);

    // ///////////////////////////////
    // /////        修改      ////////
    // ///////////////////////////////
    //按id更新品牌分类信息
    public int updateCategoryById(BrandCategory brandCategory);


    // ///////////////////////////////
    // /////        查詢      ////////
    // ///////////////////////////////
    //查询品牌分类是否存在
    public int checkCategoryByName(@Param("name") String name);

    //按ID查询品牌分类信息
    public BrandCategory findCategoryById(@Param("id") int id);

    //所有品牌分类列表
    public List<BrandCategory> getAllCategoryList();
}
