package com.flyshop.module.admin.service;

import com.flyshop.common.utils.AdminSessionUtil;
import com.flyshop.common.utils.BCryptUtil;
import com.flyshop.common.utils.IpUtils;
import com.flyshop.core.entity.DataVo;
import com.flyshop.core.entity.PageVo;
import com.flyshop.module.admin.dao.AdminDao;
import com.flyshop.module.admin.model.Admin;
import com.flyshop.module.webconfig.service.WebConfigService;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class AdminService {
    @Autowired
    private AdminDao dao;
    @Autowired
    private WebConfigService configService;

    //添加管理员信息
    @Transactional
    public DataVo addAdmin(Admin admin){
        DataVo data = DataVo.failure("操作失败");
        if(admin.getPassword()!=null){
            if(!admin.getPassword().equals(admin.getRepassword())){
                return data=DataVo.success("两次密码不一样");
            }
        }else{
            return data=DataVo.success("新用户密码不能为空！");
        }

        if(this.checkAdminByName(admin.getAdminName())){
            return data=DataVo.success("用户名已被占用！");
        }
        admin.setPassword(BCryptUtil.hashpw(admin.getPassword(), BCryptUtil.gensalt()));
        admin.setCreateAt(new Date());
        admin.setLastLoginTime(new Date());
        if(dao.addAdmin(admin)>0){
            dao.addAdminAndRole(admin.getId(),admin.getRoleId());
            return data=DataVo.success("新管理员添加成功！");
        }
        return data;
    }

    // ///////////////////////////////
    // /////       删除       ////////
    // ///////////////////////////////
    //按id删除管理员信息
    @Transactional
    public DataVo deleteAdminById(int adminId){
        DataVo data = DataVo.failure("操作失败");
        int totalCount = dao.deleteAdminById(adminId);
        if(totalCount > 0){
            dao.deleteAdminAndRole(adminId);
            data = DataVo.success("用户信息修改");
        }else{
            data=DataVo.failure("更新失败，请联系管理员！");
        }
        return data;
    }

    // ///////////////////////////////
    // /////       修改       ////////
    // ///////////////////////////////
    //更新管理员信息
    @Transactional
    public DataVo updateAdmin(Admin admin){
        DataVo data = DataVo.failure("操作失败");
        if(admin.getPassword()!=null){
            if(!admin.getPassword().equals(admin.getRepassword())){
                return data=DataVo.success("两次密码不一样");
            }
            admin.setPassword(BCryptUtil.hashpw(admin.getPassword(), BCryptUtil.gensalt()));
        }
        if(this.checkAdminByName(admin.getAdminName(),admin.getId())){
            return DataVo.failure("管理员用户名已存在！");
        }

        if(dao.updateAdmin(admin)>0){
            //先删除权限关联
            dao.deleteAdminAndRole(admin.getId());
            //重新添加权限关联信息
            dao.addAdminAndRole(admin.getId(),admin.getRoleId());
            data=DataVo.success("更新成功");
        }else{
            data=DataVo.failure("更新失败");
        }
        return data;
    }

    /**
     * 用户登陆
     *
     * @param username
     *        用户名
     * @param password
     *        密码
     * @param request
     * @throws Exception
     */
    public Admin adminLogin(String username, String password, HttpServletRequest request) {

        Admin user = dao.findByUsername(username);
        if (user != null) {
            Admin login = new Admin();
            if (BCryptUtil.checkpw(password, user.getPassword())) {
                login.setId(user.getId());
                login.setAttempts(0);
                login.setLastLoginTime(new Date());
                login.setLastLoginIp(IpUtils.getIpAddr(request));
                dao.updateAdminLogin(login);

                //用户信息写入session
                AdminSessionUtil.setLoginMember(request,user);
            }else{
                login.setId(user.getId());
                login.setAttempts(user.getAttempts()+1);
                login.setAttemptsTime(new Date());
                dao.updateAdminLogin(login);
                user = null;
            }
        }
        return user;
    }
    // ///////////////////////////////
    // /////       查询       ////////
    // ///////////////////////////////

    //按用户id查询管理员信息
    public Admin findAdminById(int adminId, int status){
        return dao.findAdminById(adminId, status);
    }

    /**
     * 通过username查询用户信息
     *
     * @param admin_name
     * @return Admin
     */
    public Admin findByUsername(String admin_name) {
        return dao.findByUsername(admin_name);
    }

    /**
     * 通过admin_name查询用户信息
     *
     * @param admin_name
     * @return
     */
    public boolean checkAdminByName(String admin_name) {
        int totalCount = dao.checkAdminByName(admin_name,null);
        return totalCount > 0 ? true : false;
    }

    /**
     * 通过admin_name查询用户信息
     *
     * @param admin_name
     * @return
     */
    public boolean checkAdminByName(String admin_name,Integer adminId) {
        int totalCount = dao.checkAdminByName(admin_name, adminId);
        return totalCount > 0 ? true : false;
    }

    /**
     * 用户翻页查询
     *
     * @param pageNum
     * @param rows
     * @return
     * @throws Exception
     */
    public PageVo<Admin> getAdminListPage(String adminName, String nickName, String mobile, String email, int pageNum, int rows) {
        PageVo<Admin> pageVo = new PageVo<Admin>(pageNum);
        pageVo.setRows(rows);
        List<Admin> list = new ArrayList<Admin>();
        pageVo.setList(dao.getAdminList(adminName, nickName, mobile, email,pageVo.getOffset(), pageVo.getRows()));
        pageVo.setCount(dao.getAdminCount(adminName, nickName, mobile, email));
        return pageVo;
    }
}
