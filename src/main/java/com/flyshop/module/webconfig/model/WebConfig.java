package com.flyshop.module.webconfig.model;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
public class WebConfig implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer id;
    private Integer typebase;
    private String keycode;
    private String keyvalue;
    private String description;
    private Integer sort;
}
