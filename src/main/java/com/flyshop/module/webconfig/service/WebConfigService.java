/*
 *	Copyright © 2015 Zhejiang SKT Science Technology Development Co., Ltd. All rights reserved.
 *	浙江斯凯特科技发展有限公司 版权所有
 *	http://www.28844.com
 */

package com.flyshop.module.webconfig.service;

import java.util.ArrayList;
import java.util.List;

import com.flyshop.core.entity.PageVo;
import com.flyshop.module.webconfig.dao.WebConfigDao;
import com.flyshop.module.webconfig.model.Guide;
import com.flyshop.module.webconfig.model.WebConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;



/**
 * 
 * 开发公司：浙江斯凯特科技发展有限公司 版权所有 <p>
 * 版权所有：© www.97560.com<p>
 * 博客地址：http://www.97560.com  <p>
 * <p>
 * 
 * 用户服务类
 * 
 * <p>
 * 
 * 区分　责任人　日期　　　　说明<br/>
 * 创建　孙开飞　2017年11月12日 　用户模块所有服务类操作函数<br/>
 * <p>
 * *******
 * <p>
 * 
 * @author sun-kaifei
 * @email 79678111@qq.com
 * @version 1.0,2017年11月12日 <br/>
 * 
 */
@Service
public class WebConfigService {

	@Autowired
	private WebConfigDao dao;

	// ///////////////////////////////
	// /////       增加       ////////
	// ///////////////////////////////

	/**
	 * 增加配置
	 * 
	 * @param webConfig
	 * @return Config
	 */
	public void addConfig(WebConfig webConfig) {
		dao.addConfig(webConfig);
	}

	//添加设置导航信息
	public void addGuide(String name,String link,Integer sort) {
		Guide guide=new Guide();
		guide.setName(name);
		guide.setLink(link);
		guide.setSort(sort);
		dao.addGuide(guide);
	}
	// ///////////////////////////////
	// ///// 刪除 ////////
	// ///////////////////////////////

	/**
	 * 删除配置
	 * 
	 * @param keycode
	 * @return Integer
	 */
	public int deleteConfigByKey(String keycode) {
	    return dao.deleteConfig(keycode);
	}

	// ///////////////////////////////
	// ///// 修改 ////////
	// ///////////////////////////////

	/**
	 * 更新配置
	 * 
	 * @param config
	 */
	public int updagteConfigByKey(WebConfig config) {
		return dao.updagteConfigByKey(config);
	}
	
	// ///////////////////////////////
	// /////       查询       ////////
	// ///////////////////////////////

	/**
	 * @param key
	 * @return
	 */
	public String getStringByKey(String key) {
		WebConfig config = dao.getConfigByKey(key);
		if (config == null) {
			return "";
		} else {
			return config.getKeyvalue();
		}
	}

	/**
	 * @param key
	 * @return
	 */
	public int getIntKey(String key) {
		WebConfig config = dao.getConfigByKey(key);
		if (config == null) {
			return 0;
		} else {
			return Integer.parseInt("1");
		}
	}
	
	@Cacheable(value="config")
	public WebConfig getConfigByKey(String key) {
		return dao.getConfigByKey(key);
	}
	
	/**
	 * 配置信息总数
	 * 
	 * @return
	 */
	public int getConfigCount(){
		return dao.getConfigCount();
	}
	
	/**
	 * 配置信息翻页列表
	 * 
	 * @param offset
	 * @param rows
	 * @return
	 * @throws Exception
	 */
	public List<WebConfig> getConfigList(int offset, int rows) throws Exception{
		List<WebConfig> groupList = dao.getConfigList(offset, rows);
		return groupList;
	}
	
	
	
	/**
	 * 查看配置列表分页
	 * 
	 * @return List<Config>
	 * @throws Exception 
	 */
	public PageVo<WebConfig> getConfigVoPage(int pageNum, int rows) throws Exception{
		PageVo<WebConfig> pageVo = new PageVo<WebConfig>(pageNum);
		pageVo.setRows(rows);
		List<WebConfig> list = new ArrayList<WebConfig>();
		int count = 0;
		count = this.getConfigCount();
		pageVo.setList(this.getConfigList(pageVo.getOffset(), pageVo.getRows()));
		pageVo.setCount(count);
		return pageVo;
	}

	/**
	 * 所有配置列表信息
	 *
	 * @return
	 * @throws Exception
	 */
	@Cacheable(value = "config")
	public List<WebConfig> getConfigAllList(){
		return dao.getConfigAllList();
	}

}
