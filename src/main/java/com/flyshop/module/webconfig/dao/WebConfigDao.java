package com.flyshop.module.webconfig.dao;

import com.flyshop.module.security.model.Permission;
import com.flyshop.module.webconfig.model.Guide;
import com.flyshop.module.webconfig.model.WebConfig;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WebConfigDao {

    // ///////////////////////////////
    // ///// 增加 ////////
    // ///////////////////////////////
    //添加配置信息
    public int addConfig(WebConfig config);

    //添加设置导航信息
    public int addGuide(Guide guide);
    // ///////////////////////////////
    // ///// 刪除 ////////
    // ///////////////////////////////
    /**
     * 删除配置
     *
     * return Integer
     */
    public int deleteConfig(@Param("keycode") String keycode);

    // ///////////////////////////////
    // ///// 修改 ////////
    // ///////////////////////////////
    public int updagteConfigByKey(WebConfig config);

    // ///////////////////////////////
    // ///// 查詢 ////////
    // ///////////////////////////////

    //按key查询配置信息
    public WebConfig getConfigByKey(@Param("keycode") String keycode);

    public List<WebConfig> getConfigList(int offset, int rows);


    public int getConfigCount();

    //所有配置列表信息
    public List<WebConfig> getConfigAllList();
}
