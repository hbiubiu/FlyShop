package com.flyshop.module.user.model;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.math.BigDecimal;
@Setter
@Getter
public class UserGroup implements Serializable {
    private Integer id;
    @NotEmpty(message="用户组名不能为空")
    private String groupName;

    private BigDecimal discount;

    private Integer minexp;

    private Integer maxexp;

    private String messageIds;

    private static final long serialVersionUID = 1L;

}