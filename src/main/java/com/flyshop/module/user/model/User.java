package com.flyshop.module.user.model;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Setter
@Getter
public class User implements Serializable {
    private static final long serialVersionUID = 1L;
    //用户id
    private Integer userId;
    //用户名
    private String username;
    //密码
    private String password;
    //确认密码
    private String repassword;
    //姓名
    private String trueName;
    //用户头像
    private String avatar;
    //座机号码
    private String telephone;
    //手机号码
    private String mobile;
    //省份id
    private Integer province;
    //地区id
    private Integer city;
    //县市id
    private Integer area;
    //详细地址
    private String contactAddr;
    //邮政编码
    private String zip;
    //联系qq号码
    private String qq;
    //性别
    private Integer sex;
    //生日
    private Date birthday;
    //所在的会员组
    private Integer groupId;
    //经验值
    private Integer exp;
    //积分
    private Integer point;
    private String messageIds;
    private Date time;

    private Integer status;
    private String prop;
    private BigDecimal balance;
    //成功登陆最后时间
    private Date lastLogin;
    private String custom;
    //联系邮箱
    private String email;
    //成功登陆最后ip
    private String loginIp;
    //尝试登陆次数
    private Integer attempts;
    //尝试登陆最后时间
    private Date attemptsTime;
}
