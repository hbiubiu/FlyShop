package com.flyshop.module.user.service;

import com.flyshop.core.entity.DataVo;
import com.flyshop.core.entity.PageVo;
import com.flyshop.module.user.dao.UserGroupDao;
import com.flyshop.module.user.model.UserGroup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 开发公司：28844.com<br/>
 * 版权：28844.com<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 12:14 2018/7/9
 */
@Service
public class UserGroupService {
    @Autowired
    private UserGroupDao dao;
    // ///////////////////////////////
    // /////      增加        ////////
    // ///////////////////////////////
    //添加用户组
    public DataVo addUserGroup(UserGroup group){
        DataVo data = DataVo.failure("操作失败");
        int total = dao.addUserGroup(group);
        if(total>0){
            data=DataVo.success("操作成功");
        }else{
            data=DataVo.failure("添加失败");
        }
        return data;
    }
    // ///////////////////////////////
    // /////        删除      ////////
    // ///////////////////////////////

    // ///////////////////////////////
    // /////       修改       ////////
    // ///////////////////////////////

    // ///////////////////////////////
    // /////       查询       ////////
    // ///////////////////////////////
    //按id查询用户组信息
    public UserGroup findUserGroupByid(int id){
        return dao.findUuserGroupByid(id);
    }

    /**
     * 用户组翻页查询
     *
     * @param pageNum
     * @param rows
     * @return
     * @throws Exception
     */
    public PageVo<UserGroup> getUserGroupListPage(int pageNum, int rows) {
        PageVo<UserGroup> pageVo = new PageVo<UserGroup>(pageNum);
        pageVo.setRows(rows);
        List<UserGroup> list = new ArrayList<UserGroup>();
        pageVo.setList(dao.getUserGroupList(pageVo.getOffset(), pageVo.getRows()));
        pageVo.setCount(dao.getUserGroupCount());
        return pageVo;
    }

    //所有用户组列表
    public List<UserGroup> getAllUserGroupList(){
        return  dao.getAllUserGroupList();
    };
}
