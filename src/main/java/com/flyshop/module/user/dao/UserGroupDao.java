package com.flyshop.module.user.dao;

import com.flyshop.module.user.model.UserGroup;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 开发公司：28844.com<br/>
 * 版权：28844.com<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 10:07 2018/7/9
 */
@Repository
public interface UserGroupDao {

    // ///////////////////////////////
    // /////       增加       ////////
    // ///////////////////////////////
    //添加用户组
    public int addUserGroup(UserGroup group);

    // ///////////////////////////////
    // /////        刪除      ////////
    // ///////////////////////////////

    // ///////////////////////////////
    // /////        修改      ////////
    // ///////////////////////////////


    // ///////////////////////////////
    // ///// 查詢 ////////
    // ///////////////////////////////
    //按id查询用户组信息
    public UserGroup findUuserGroupByid(@Param("id") int id);

    //查询用户组总数
    public int getUserGroupCount();

    //用户组列表
    public List<UserGroup> getUserGroupList(@Param("offset") int offset, @Param("rows") int rows);

    //所有用户组列表
    public List<UserGroup> getAllUserGroupList();
}
