package com.flyshop.module.user.dao;

import com.flyshop.module.user.model.User;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

@Repository
public interface UserDao {

    // ///////////////////////////////
    // ///// 增加 ////////
    // ///////////////////////////////
    public int addUser(User user);

    // ///////////////////////////////
    // /////        刪除      ////////
    // ///////////////////////////////
    //按id删除用户信息
    public int deleteUserById(@Param("userId") int userId);

    // ///////////////////////////////
    // /////        修改      ////////
    // ///////////////////////////////
    //更新用户登录ip地址和登录时间
    public int updateUserLogin(User user);

    //按id更新用户信息
    public int updateUser(User user);
    // ///////////////////////////////
    // /////       查询       ////////
    // ///////////////////////////////
    /**
     * 通过userId查询用户信息
     * 用户状态0是所有 1未审核 2正常状态 3 删除至回收站 4锁定
     *
     * @param userId
     *         用户id
     * @param status
     *         审核状态
     * @return User
     */
    public User findUserById(@Param("userId") int userId,@Param("status") int status);

    /**
     * 通过username查询用户信息
     *
     * @param username
     * @return User
     */
    public User findByUsername(@Param("username") String username);

    //查询用户组总数
    public int getUserCount(@Param("username") String username,
                            @Param("trueName") String trueName,
                            @Param("mobile") String mobile,
                            @Param("email") String email);

    //用户组列表
    public List<User> getUserList(@Param("username") String username,
                                  @Param("trueName") String trueName,
                                  @Param("mobile") String mobile,
                                  @Param("email") String email,
                                  @Param("offset") int offset,
                                  @Param("rows") int rows);
}
