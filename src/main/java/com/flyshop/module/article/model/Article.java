package com.flyshop.module.article.model;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Setter
@Getter
public class Article implements Serializable {
    private Integer id;

    private String title;

    private Integer categoryId;

    private Date createTime;

    private String keywords;

    private String description;

    private Integer visibility;

    private Integer top;

    private Integer sort;

    private Integer style;

    private String color;

    private String content;

    private static final long serialVersionUID = 1L;
}