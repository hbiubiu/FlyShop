package com.flyshop.module.article.model;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
public class ArticleCategory implements Serializable {
    private Integer id;

    private String name;

    private String categoryId;

    private Integer parentId;

    private Integer issys;

    private Integer sort;

    private String path;

    private static final long serialVersionUID = 1L;

}