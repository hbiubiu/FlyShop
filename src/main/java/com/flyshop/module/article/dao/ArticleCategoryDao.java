package com.flyshop.module.article.dao;

import com.flyshop.module.article.model.ArticleCategory;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 开发公司：28844.com<br/>
 * 版权：28844.com<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 9:24 2018/7/13
 */
@Repository
public interface ArticleCategoryDao {


    // ///////////////////////////////
    // /////       增加       ////////
    // ///////////////////////////////
    //添加文章分类
    public int addArticleCategory(ArticleCategory articleCategory);


    // ///////////////////////////////
    // /////        刪除      ////////
    // ///////////////////////////////

    // ///////////////////////////////
    // /////        修改      ////////
    // ///////////////////////////////



    // ///////////////////////////////
    // /////        查詢      ////////
    // ///////////////////////////////
    //查询文章分类名是否存在
    public int checkArticleCategoryByName(@Param("name") String name);

    //查询所有文章类目数量
    public int getArticleCategoryCount();

    //文章类目列表
    public List<ArticleCategory> getArticleCategoryList(@Param("offset") Integer offset,
                                                @Param("rows") Integer rows);

    public List<ArticleCategory> getArticleCategoryListByFatherId(@Param("id") Integer id);

}
