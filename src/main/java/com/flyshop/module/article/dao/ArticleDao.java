package com.flyshop.module.article.dao;

import com.flyshop.module.article.model.Article;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 开发公司：28844.com<br/>
 * 版权：28844.com<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 9:24 2018/7/13
 */
@Repository
public interface ArticleDao {

    // ///////////////////////////////
    // /////       增加       ////////
    // ///////////////////////////////
    //添加文章
    public int addArticle(Article article);


    // ///////////////////////////////
    // /////        刪除      ////////
    // ///////////////////////////////

    // ///////////////////////////////
    // /////        修改      ////////
    // ///////////////////////////////



    // ///////////////////////////////
    // /////        查詢      ////////
    // ///////////////////////////////

    //查询文章标题是否存在
    public int checkArticleByTitle(@Param("title") String title);

    //查询所有文章数量
    public int getArticleCount(@Param("title") String title,
                             @Param("createTime") String createTime);

    //文章列表
    public List<Article> getArticleList(@Param("title") String title,
                                      @Param("createTime") String createTime,
                                      @Param("offset") Integer offset,
                                      @Param("rows") Integer rows);
}
