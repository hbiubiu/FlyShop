package com.flyshop.module.article.service;

import com.flyshop.core.entity.DataVo;
import com.flyshop.core.entity.PageVo;
import com.flyshop.module.article.dao.ArticleDao;
import com.flyshop.module.article.model.Article;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 开发公司：28844.com<br/>
 * 版权：28844.com<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 10:23 2018/7/13
 */
@Service
public class ArticleService {
    @Autowired
    protected ArticleDao dao;

    // ///////////////////////////////
    // /////      增加        ////////
    // ///////////////////////////////
    //添加文章
    public DataVo addArticle(Article article){
        DataVo data = DataVo.failure("操作失败");
        if(StringUtils.isBlank(article.getTitle())){
            return data=DataVo.failure("标题不能为空！");
        }
        if(StringUtils.isBlank(article.getContent())){
            return data=DataVo.failure("内容不能为空！");
        }
        if(this.checkArticleByTitle(article.getTitle())){
            return data=DataVo.failure("标题已存在！");
        }
        article.setCreateTime(new Date());
        int totalCount=dao.addArticle(article);
        if(totalCount > 0){
            data = DataVo.success("文章添加成功！");
        }else{
            data=DataVo.failure("添加失败！");
        }
        return data;
    }

    // ///////////////////////////////
    // /////        刪除      ////////
    // ///////////////////////////////

    // ///////////////////////////////
    // /////        修改      ////////
    // ///////////////////////////////

    // ///////////////////////////////
    // /////       查询       ////////
    // ///////////////////////////////
    //检查文章标题是否存在
    public boolean checkArticleByTitle(String title) {
        int totalCount = dao.checkArticleByTitle(title);
        return totalCount > 0 ? true : false;
    }

    /**
     * 文章翻页查询
     *
     * @param title
     *         标题
     * @param createTime
     *         添加时间
     * @param pageNum
     *         当前页码
     * @param rows
     *         每页数量
     * @return
     */
    public PageVo<Article> getArticleListPage(String title,String createTime,int pageNum, int rows) {
        PageVo<Article> pageVo = new PageVo<Article>(pageNum);
        pageVo.setRows(rows);
        List<Article> list = new ArrayList<Article>();
        pageVo.setList(dao.getArticleList(title,createTime,pageVo.getOffset(), pageVo.getRows()));
        pageVo.setCount(dao.getArticleCount(title,createTime));
        return pageVo;
    }
}
