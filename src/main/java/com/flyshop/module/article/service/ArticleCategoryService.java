package com.flyshop.module.article.service;

import com.flyshop.core.entity.DataVo;
import com.flyshop.core.entity.PageVo;
import com.flyshop.module.article.dao.ArticleCategoryDao;
import com.flyshop.module.article.model.ArticleCategory;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * 开发公司：28844.com<br/>
 * 版权：28844.com<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 10:23 2018/7/13
 */
@Service
public class ArticleCategoryService {
    @Autowired
    protected ArticleCategoryDao articleCategoryDao;

    // ///////////////////////////////
    // /////      增加        ////////
    // ///////////////////////////////
    public DataVo addArticleCategory(ArticleCategory articleCategory){
        DataVo data = DataVo.failure("操作失败");
        if(StringUtils.isBlank(articleCategory.getName())){
            return data=DataVo.failure("栏目名不能为空！");
        }
        if(this.checkArticleByTitle(articleCategory.getName())){
            return data= DataVo.success("栏目名称以重复！");
        }

        int totalCount=articleCategoryDao.addArticleCategory(articleCategory);
        if(totalCount > 0){
            data = DataVo.success("栏目添加成功！");
        }else{
            data=DataVo.failure("添加失败！");
        }
        return data;
    };

    // ///////////////////////////////
    // /////        刪除      ////////
    // ///////////////////////////////

    // ///////////////////////////////
    // /////        修改      ////////
    // ///////////////////////////////

    // ///////////////////////////////
    // /////       查询       ////////
    // ///////////////////////////////
    //检查栏目名称是否存在
    public boolean checkArticleByTitle(String name) {
        int totalCount = articleCategoryDao.checkArticleCategoryByName(name);
        return totalCount > 0 ? true : false;
    }

    /**
     * 文章类目翻页查询
     *
     * @param pageNum
     *         当前页码
     * @param rows
     *         每页数量
     * @return
     */
    public PageVo<ArticleCategory> getArticleCategoryListPage(int pageNum, int rows) {
        PageVo<ArticleCategory> pageVo = new PageVo<ArticleCategory>(pageNum);
        pageVo.setRows(rows);
        List<ArticleCategory> list = new ArrayList<ArticleCategory>();
        pageVo.setList(articleCategoryDao.getArticleCategoryList(pageVo.getOffset(), pageVo.getRows()));
        pageVo.setCount(articleCategoryDao.getArticleCategoryCount());
        return pageVo;
    }

    public  List<ArticleCategory> getArticleCategoryListByFatherId(Integer id){
        return articleCategoryDao.getArticleCategoryListByFatherId(id);
    }
}
