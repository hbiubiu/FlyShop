package com.flyshop.config;

import com.flyshop.interceptor.AdminInterceptor;
import com.flyshop.interceptor.UserInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.*;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import javax.annotation.Resource;
import java.util.Locale;


@Configuration
public class WebMvcConfig extends WebMvcConfigurationSupport{

	@Resource
	private AdminInterceptor interceptor;

	@Resource
	private UserInterceptor userInterceptor;

	@Override
	public void configurePathMatch(PathMatchConfigurer configurer) {
		configurer.setUseSuffixPatternMatch(false);
	}


	@Bean
	public LocaleResolver localeResolver() {
		SessionLocaleResolver slr = new SessionLocaleResolver();
		// 默认语言
		slr.setDefaultLocale(Locale.CHINA);
		return slr;
	}

	@Bean
	public LocaleChangeInterceptor localeChangeInterceptor() {
		LocaleChangeInterceptor lci = new LocaleChangeInterceptor();
		// 参数名
		lci.setParamName("lang");
		return lci;
	}

	//添加拦截器
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(interceptor).addPathPatterns("/admin/**")
				.excludePathPatterns("/*",
						"/admin/login",
						"/admin/logout",
						"/admin/login_act");

		registry.addInterceptor(userInterceptor).addPathPatterns("/ucenter/**")
				.excludePathPatterns("/*",
						"/ucenter/login",
						"/ucenter/unauthorized",
						"/ucenter/reg",
						"/ucenter/forgot",
						"/ucenter/logout",//退出登录
						"/ucenter/login_act",//登录处理
						"/ucenter/reg_user",
						"/ucenter/reset.json",
						"/ucenter/logintip",
						"/ucenter/mailcaptcha.json");
		registry.addInterceptor(localeChangeInterceptor());
	}

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		//配置静态资源处理
		registry.addResourceHandler("/**")
				.addResourceLocations("file:./uploadfiles/")
				.addResourceLocations("file:./views/static/");
	}
}
