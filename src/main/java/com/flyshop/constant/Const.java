/*
 *	Copyright © 2015 Zhejiang SKT Science Technology Development Co., Ltd. All rights reserved.
 *	浙江斯凯特科技发展有限公司 版权所有
 *	http://www.28844.com
 */
package com.flyshop.constant;


/**
 * 
 * 开发公司：97560.com<br/>
 * 版权：97560.com<br/>
 * <p>
 * 
 * 变量设置类
 * 
 * <p>
 * 
 * 区分　责任人　日期　　　　说明<br/>
 * 创建　孙开飞　2017年10月15日 　<br/>
 * <p>
 * *******
 * <p>
 * 
 * @author sun-kaifei
 * @email admin@97560.com
 * @version 1.0<br/>
 * 
 */
public class Const {

	public static final String SYSTEM_NAME = "FlyShop";
	public static final String SYSTEM_VERSION = "1.0.0";
	/**
	 * 验证码
	 */
	public static String KAPTCHA_SESSION_KEY = "kaptcha";

	/**
	 * Session中的用户信息
	 */
	public static final String SESSION_ADMIN = "session_admin";

	/**
	 * Session中的用户信息
	 */
	public static final String SESSION_USER = "session_user";

}
