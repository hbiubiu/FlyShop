package com.flyshop.common.utils;

import com.flyshop.constant.Const;
import com.flyshop.module.admin.model.Admin;
import com.flyshop.module.user.model.User;

import javax.servlet.http.HttpServletRequest;

/**
 * 
 * 开发公司：28844.com<br/>
 * 版权：28844.com<br/>
 * <p>
 * 
 * 用户登录信息操作
 * 
 * <p>
 * 
 * 区分　责任人　日期　　　　说明<br/>
 * 创建　孙开飞　2017年5月25日 　<br/>
 * <p>
 * *******
 * <p>
 * 
 * @author sun-kaifei
 * @email admin@28844.com
 * @version 1.0 <br/>
 * 
 */
public class AdminSessionUtil {

    /**
     * 读取用户SESSION信息
     * 
     * @param request
     * @return
     */
    public static Admin getLoginMember(HttpServletRequest request){
        Admin loginMember = (Admin) request.getSession().getAttribute(Const.SESSION_ADMIN);
        return loginMember;
    }

    /**
     * 写入用户SESSION信息
     * 
     * @param request
     * @param admin
     */
    public static void setLoginMember(HttpServletRequest request,Admin admin){
        request.getSession().setAttribute(Const.SESSION_ADMIN,admin);
    }

    /**
     * 用户访问链接转跳判断
     * 
     * @param request
     * @param redirectUrl
     * @return
     */
    public static String judgeLoginJump(HttpServletRequest request,String redirectUrl){
        Admin user = getLoginMember(request);
    	if(user == null){
            String redirect = "redirect:/admin/login";
            if(!StringHelperUtil.isEmpty(redirectUrl)){
                redirect += "?redirectUrl="+request.getContextPath() + redirectUrl;
            }
            return redirect;
        }
        return null;
    }
}
