package com.flyshop.common.utils;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * 开发公司：浙江斯凯特科技发展有限公司 版权所有 <p>
 * 版权所有：© www.28844.com<p>
 * 博客地址：http://www.28844.com  <p>
 * <p>
 * 
 * Shiro Filter 工具类
 * 
 * <p>
 * 
 * 区分　责任人　日期　　　　说明<br/>
 * 创建　孙开飞　2017年7月20日 　<br/>
 * <p>
 * *******
 * <p>
 * 
 * @author sun-kaifei
 * @email i@itboy.net
 * @version 1.0,2017年7月20日 <br/>
 * 
 */
public class FilterUtils {
	private static Logger logger = LoggerFactory.getLogger(FilterUtils.class);
	final static Class<? extends FilterUtils> CLAZZ = FilterUtils.class;
	/**
	 * 是否是Ajax请求
	 * @param request
	 * @return
	 */
	public static boolean isAjax(ServletRequest request){
		return "XMLHttpRequest".equalsIgnoreCase(((HttpServletRequest) request).getHeader("X-Requested-With"));
	}
	
	/**
	 * response 输出JSON
	 * @param response
	 * @param resultMap
	 * @throws IOException
	 */
	public static void out(ServletResponse response, Map<String, String> resultMap){
		
		PrintWriter out = null;
		try {
			response.setCharacterEncoding("UTF-8");
			out = response.getWriter();
			out.println(JSONObject.fromObject(resultMap).toString());
		} catch (Exception e) {
			logger.error("输出JSON报错。" ,e);
		}finally{
			if(null != out){
				out.flush();
				out.close();
			}
		}
	}
}
