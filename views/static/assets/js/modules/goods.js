define(function(require, exports, module) {
    J = jQuery;
    require('jqueryform');
    require('layer');
    layer.config({
        path: '/assets/js/vendors/layer/' //layer.js所在的目录，可以是绝对目录，也可以是相对目录
    });

    $("#selectAll").click(function(){
        if($("input[type='checkbox']:not(:checked)").length>0) {
            $("input[type='checkbox']").prop('checked',true);
        }else {
            $("input[type='checkbox']").prop('checked',false);
        }
    });

});