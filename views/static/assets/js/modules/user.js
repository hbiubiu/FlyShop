define(function(require, exports, module) {
	J = jQuery;
    require('jqueryform');
	require('layer');
	layer.config({
	    path: '/assets/js/vendors/layer/' //layer.js所在的目录，可以是绝对目录，也可以是相对目录
	});
	var timenow = new Date().getTime();

    $("#group_add").ajaxForm({
        dataType: "json"
        , beforeSubmit: function(formData, jqForm, options) {}
        , success: function(ret) {
            if (ret.code == 0) {
                layer.msg("添加成功", { shift: -1 }, function () {
                    location.href = "/admin/user/group_list";
                });
            } else {
                layer.msg(ret.message, {icon: 2});
            }
        }
        , error: function(ret) {alert(ret.message);}
        , complete: function(ret) {} 	      // 无论是 success 还是 error，最终都会被回调
    });

    //地区选择下拉菜单
    $('#province').change(function () {
        $('#city option:gt(0)').remove();
        $('#area option:gt(0)').remove();
        var parentId=$(this).val();
        $.ajax({
            type: "post",
            dataType:"json",
            url: "/areas/area_child",
            data: {"parentId":parentId},
            success: function (result) {
                var strocity = '';
                var datas = eval(result);
                $.each(datas, function(i,val){
                    strocity += "<option value='"+val.areaId+"' >"+val.areaName+"</option>";
                });
                $('#city').append(strocity);
            }
        })
    });

    //县级下来菜单
    $('#city').change(function () {
        $('#area option:gt(0)').remove();
        var parentId=$(this).val();
        $.ajax({
            type: "post",
            dataType:"json",
            url: "/areas/area_child",
            data: {"parentId":parentId},
            success: function (result) {
                var stroarea = '';
                var datas = eval(result);
                $.each(datas, function(i,val){
                    stroarea += "<option value='"+val.areaId+"' >"+val.areaName+"</option>";
                });
                $('#area').append(stroarea);
            }
        })
    });

    //删除用户信息
    $(document).on('click', '.user-delete', function (){
        var id = $(this).attr("data-id");
        var title = $(this).attr("data-title");
        layer.confirm('您是确定删除《'+title+'》？删除后将无法恢复！', {
            btn: ['确定','取消'] //按钮
        }, function(){
            $.ajax({
                url: "/admin/user/del?"+Math.random(),
                data: {"id":id},
                dataType: "json",
                type :  "POST",
                cache : false,
                async: false,
                error : function(i, g, h) {
                    layer.msg('发送错误', {icon: 2});
                },
                success: function(ret){
                    if (ret.code >= 0) {
                        layer.msg("删除成功！", {icon: 1});
                        window.location.reload();
                        return false;
                    } else {
                        layer.msg(ret.message, {icon: 5});
                        return false;
                    }
                }
            });
        }, function(){
        });
    });

    $("#admin_add").ajaxForm({
        dataType: "json"
        , beforeSubmit: function(formData, jqForm, options) {}
        , success: function(ret) {
            if (ret.code == 0) {
                layer.msg(ret.message, { shift: -1 }, function () {
                    location.href = "/admin/user/admin_list";
                });
            } else {
                layer.msg(ret.message, {icon: 2});
            }
        }
        , error: function(ret) {alert(ret.message);}
        , complete: function(ret) {} 	      // 无论是 success 还是 error，最终都会被回调
    });

    //删除用户信息
    $(document).on('click', '.admin-delete', function (){
        var id = $(this).attr("data-id");
        var title = $(this).attr("data-title");
        layer.confirm('您是确定删除《'+title+'》管理员？删除后将无法恢复！', {
            btn: ['确定','取消'] //按钮
        }, function(){
            $.ajax({
                url: "/admin/user/delAdmin?"+Math.random(),
                data: {"id":id},
                dataType: "json",
                type :  "POST",
                cache : false,
                async: false,
                error : function(i, g, h) {
                    layer.msg('发送错误', {icon: 2});
                },
                success: function(ret){
                    if (ret.code >= 0) {
                        layer.msg("删除成功！", {icon: 1});
                        window.location.reload();
                        return false;
                    } else {
                        layer.msg(ret.message, {icon: 5});
                        return false;
                    }
                }
            });
        }, function(){

        });
    });
});