define(function(require, exports, module) {

	require('umeditor.config');
	require('umeditor');
	var serverPath = "/",
	umEditor = UM.getEditor('content', {
		imageUrl: "/member/upload",
    	imagePath: serverPath,
        
        toolbar: ["source", "|","undo", "redo", "|", "bold", "italic", "underline", "fontfamily fontsize" ,"|", "justifyleft", "justifycenter",
            "justifyright", "|", "link", "unlink", "|", "insertorderedlist", "insertunorderedlist", 
			"|",'blockquote', "emotion", "image", "removeformat","|", "horizontal print preview fullscreen", "drafts", "formula"],
        wordCount: true,
        maximumWords: 20000,
        initialFrameWidth: '100%',
        initialFrameHeight: 300,
        initialContent:'请在这里发表您要撰写的文章！',
        autoClearinitialContent: true,
        autoHeightEnabled: true,
        focus: false,
        enableAutoSave: true,
        saveInterval: 500,
        autoFloatEnabled: false,
        allHtmlEnabled:false,
        scaleEnabled:true
    });
});