/*
 * Copyright (c) 2003-2010, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */
CKEDITOR.config.height = 450;
CKEDITOR.config.width = 'auto';

CKEDITOR.editorConfig = function(config) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	config.language = 'zh-cn';
	//config.filebrowserBrowseUrl = 'ckeditor/uploader/browse.jsp';
	//config.filebrowserImageBrowseUrl = 'ckeditor/uploader/browse.jsp?type=Images';
	//config.filebrowserFlashBrowseUrl = 'ckeditor/uploader/browse.jsp?type=Flashs';
	//config.filebrowserUploadUrl = 'ckeditor/uploader/upload.jsp';
	config.filebrowserImageUploadUrl = '/member/upload';
	//config.filebrowserFlashUploadUrl = 'ckeditor/uploader/upload.jsp?type=Flashs';
	config.filebrowserWindowWidth = 'auto';
	config.filebrowserWindowHeight = '680';
	config.toolbar_A = [
			['Source'],
			['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-',
					'SpellChecker', 'Scayt'],
			['Undo', 'Redo', '-', 'Find', 'Replace', '-', 'SelectAll'],
			['Bold', 'Italic', 'Underline', 'Strike', '-', 'Subscript',
					'Superscript','-', 'RemoveFormat'],
			['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent',
					'Blockquote'],
			['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
			['Link', 'Unlink'],
			['Image', 'HorizontalRule', 'Smiley',
					'SpecialChar'], '/',
			['Styles', 'Format', 'Font', 'FontSize'], ['TextColor', 'BGColor']];
	config.toolbar = 'A';
	config.removePlugins = 'elementspath,resize'; // 移除编辑器底部状态栏显示的元素路径和调整编辑器大小的按钮
	config.allowedContent = true; // 是否允许使用源码模式进行编辑
	config.forcePasteAsPlainText = false; // 是否强制复制过来的文字去除格式
	config.enterMode = CKEDITOR.ENTER_BR; // 编辑器中回车产生的标签CKEDITOR.ENTER_BR(<br>),CKEDITOR.ENTER_P(<p>),CKEDITOR_ENTER(回车)
	// 设置快捷键
	// 用于实现Ctrl + V进行粘贴
	// 无此配置，无法进行快捷键粘贴
	//config.keystrokes = [
	//    [CKEDITOR.CTRL + 86 /* V */, 'paste']
	//];

	// 设置快捷键，可能与浏览器冲突plugins/keystrokes/plugin.js
	// 用于实现Ctrl + V进行粘贴
	// 此配置将会启动粘贴之前进行过滤，若无此配置，将会出现粘贴之后才弹出过滤框
	//config.blockedKeystrokes = [
	//    CKEDITOR.CTRL + 86
	//];
};
