define(function(require, exports, module) {
    var plugins = require('plugins');
    J = jQuery;
    require('layer');
    require('bootstrap');
    require('jqueryform');
    require('FengFocus');
    require('FengTab');
    require('marquee');
    require('jquery-ui');

    layer.config({
        path: '/assets/js/vendors/layer/' //layer.js所在的目录，可以是绝对目录，也可以是相对目录
    });

    //dom载入完毕执行
    $(function(){
        // 调用焦点图
        $("#home_fouse").FengFocus({trigger : "mouseover"});
        $('#home_rec').kxbdSuperMarquee({
            distance: 218,
            time: 500,
            btnGo: {left:'#home_rec_left',right:'#home_rec_right'},
            direction: 'left'
        });
        // 调用焦点图
        $('#home_panic').kxbdSuperMarquee({
            distance: 1200,
            time: 500,
            btnGo: {left:'#home_panic_left',right:'#home_panic_right'},
            direction: 'left'
        });
        //显示抢购倒计时
        var cd_timer = new countdown();
        cd_timer.add(2);
        cd_timer.add(14);
        cd_timer.add(15);
        cd_timer.add(16);
    });

    //DOM加载完毕后运行
    $(function(){
        //隔行换色
        $(".list_table tr:nth-child(even)").addClass('even');
        $(".list_table tr").hover(
            function () {
                $(this).addClass("sel");
            },
            function () {
                $(this).removeClass("sel");
            }
        );

        //按钮高亮
        var localUrl = "/ucenter/index";
        $('a[href^="'+localUrl+'"]').parent().addClass('current');
    });

    /*验证码重新加载*/
    $("#reloadCaptcha").click(function(){
        new_src = '/captcha/default?'+Math.random();
        $(this).find("img").attr("src",new_src);
    });

    $("#myloginForm").ajaxForm({
        dataType: "json"
        , beforeSubmit: function(formData, jqForm, options) {}
        , success: function(ret) {
            if (ret.code == 0) {
                location.href = ret.url;
            } else {
                layer.msg(ret.message, {icon: 2});
            }
        }
        , error: function(ret) {alert(ret.message);}
        , complete: function(ret) {} 	      // 无论是 success 还是 error，最终都会被回调
    });

});