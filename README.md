# FlyShop

#### 项目介绍
FlyShop是国内第一个基于JAVA编程语言开发的开源网店购物系统，国际化（I18N，目前支持中文、英文，通过添加语言文件还可支持更多语言），采用Spring Boot、mybatis、freemark、Bootstrap等开源技术和自主框架技术开发。以其开源、免费；安全、稳定；强大、易用；高效、专业等优势占据了国内JAVA电子商务系统领域的主导地位，提供在线帮助、视频教程、支持论坛、在线客服等多种完善的技术支持和服务，定位营销型、互动型、赢利型、安全、稳定的企业级网上商城系统。

核心框架：Spring Boot 2.0.3<br />
持久层框架：mybatis 3.4.6<br />
定时器：Quartz 2.3<br />
数据库连接池：Druid 1.1.5<br />
日志管理：SLF4J 1.7、Log4j<br />

#### 目前完成说明
目前正在开发管理后台中！ **QQ交流群：211378508** 

### 前台
##### 首页
* 网站LOGO--网站标志形象，点击LOGO图片可以快速回到首页<br />
* 商家管理--商家登录，进入商家后台，商家进行商品和订单管理操作<br />
* 申请开店--商家进入注册，后台审核成功，就可以在平台上卖自己商品<br />
* 使用帮助--网站相关帮助资讯，介绍网站功能和购物流程等<br />
* 个人中心--进入我的个人中心，查看自己的订单，积分，我的收藏，个人资料等相关<br />
* 我的购物车--查看购物车里的商品，可以进行结算<br />
* 商品搜索--可根据输入的关键词在全站商品，并列出想要查找的商品<br />
* 全部商品分类-显-示出网站全部商品分类。显示一级分类。鼠标经过显示二级或三级分类<br />
* 网站主导航--列出本站重要模块功能的快速链接，可快速直达（网站导航可在管理员后台自定义导航栏配置）<br />
* 登陆和注册--用户进入登陆和注册页面，注册成功后可有更多权限和功能<br />
* 首页幻灯片广告--在后台设置首页幻灯片广告，把网站的主要活动和商品在首页突出显示<br />
* 限时商品--显示后台促销活动的价格，在时间内特价购买<br />
* 网站资讯--显示网站最新的动态与资讯，与后台文章管理系统关联<br />
* 网站公告--对本店近期的状况给出说明，或假期说明，或促销信息与后台公告管理系统关联<br />
* 热卖商品--显示热卖商品（可后台添加指定）<br />
* 商品推荐--显示推荐商品（可后台添加指定）<br />
* 新品上架--显示新品上架商品（可后台添加指定）<br />
* 特价商品--显示特价商品（可后台添加指定）<br />
* 网站商品展示--按照商品分类进行商品展示，会显示一级分类的商品，并显示二级分类，点击分类可以进入分类查看对应的商品<br />
* 网站底部帮助中心--在后台帮助中心设置相关帮助信息<br />
* 页脚底部信息--显示网站版权信息，关于我们等，相关数据可以在后台设置<br />
* 限时抢购活动--根据网站后台营销模块设置的<br />
* 积分兑换商品--根据网站后台营销模块设置的<br />
* 邮箱订阅--用户输入邮箱可以订阅商城的咨询信息<br />


#### 技术选型：

* JDK8
* MySQL
* Spring-boot
* mybatis
* Ehcache
* Freemarker
* Bootstrap
* SeaJs
* i18n


#### 打包部署开发环境

- 将项目config目录里的webdata.properties数据库名，用户名，密码设置成你自己的
- 导入doc目录里flyshop.sql导入到你MySql里进行执行
- 运行 `mvn clean compile package`
- 开发者用户将 target目录下的FlyShop.jar和config、uploadfiles、views三个目录到你想存放的地方
- 直接下载运行包的用户忽略上一步操作，直接将压缩包解压到自己存放网站目录下按下面操作
- 运行 `java -jar FlyShop.jar --spring.profiles.active=prod > logs/FlyShop.log 2>&1 &` 项目就在后台运行了
- 关闭服务运行 `ps -ef | grep FlyShop.jar | grep -v grep | cut -c 9-15 | xargs kill -s 9`
- 查看日志运行 `tail -200f logs/FlyShop.log`

#### 目录结构说明：
    1. 程序打包后，config(数据库配置文件)、uploadfiles（图片上传目录）、views（静态资源和模板，静态如：css、js，模板后台模板前台PC和移动端）、FlyShop.jar
    2. 配置和模板写在包外是为了让不懂java的也可以使用，只要会HTML就可以编写模板，输入运行命令直接就可以运行了

### 图片演示 
*权限分配
![权限分配](doc/权限分配.png "权限分配.png")

### 在线文档

- [JDK7英文文档](http://tool.oschina.net/apidocs/apidoc?api=jdk_7u4 "JDK7英文文档")

- [Spring4.x文档](http://spring.oschina.mopaas.com/ "Spring4.x文档")

- [Mybatis3官网](http://www.mybatis.org/mybatis-3/zh/index.html "Mybatis3官网")

- [Nginx中文文档](http://tool.oschina.net/apidocs/apidoc?api=nginx-zh "Nginx中文文档")

- [Freemarker在线手册](http://freemarker.foofun.cn/ "Freemarker在线中文手册")

- [Bootstrap在线手册](http://www.bootcss.com/ "Bootstrap在线手册")

- [Git官网中文文档](https://git-scm.com/book/zh/v2 "Git官网中文文档")


## 许可证

[MIT](LICENSE "MIT")